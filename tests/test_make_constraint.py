from unittest import TestCase

from common.ConstraintSolver import *
#from common.ConstraintSolver import make_constraint
from common.likelyinvariant import *


class TestMake_constraint(TestCase):
    def test_make_constraint(self):
        invs = set()
        invs.add(LikelyInvariant([Parameter(0, "a", int), Constant(0)], ">"))
        invs.add(LikelyInvariant([Parameter(0, "a", int), Parameter(1, "b", int)], '=='))

        constraint = ConstraintSolver.make_constraint(invs)
        print("constraint: " + constraint)

        cs = ConstraintSolver()
        sol = cs.solve_constraint(constraint, [int, int], [(1,1)])

        print(sol)

