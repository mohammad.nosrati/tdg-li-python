from unittest import TestCase

from common.ConstraintSolver import *


class TestSolve_constraint(TestCase):
    def test_solve_constraint(self):
        print("\n\nsolve_constraint")
        cons = "param_0 > 0, param_0 > param_1 + 11, param_2 == param_1 + 5"
        params = [int, int, int]

        found_values = set()
        found_values.add((1, -11, -6))
        found_values.add((2, -10, -5))

        cs = ConstraintSolver()
        solution = cs.solve_constraint(cons, params, found_values)
        print(solution)


        self.assertEquals(solution[2], solution[1] + 5)

    def test_unsat(self):
        print("\n\nunsat")
        cons = "param_0 > 0, param_0 < 0"
        params = [int]

        cs = ConstraintSolver()
        solution = cs.solve_constraint(cons, params, [])
        print(solution)

        self.assert_(solution is None)

