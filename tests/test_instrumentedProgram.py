from unittest import TestCase

from common.InstrumentedProgram import *
from common.likelyinvariant import LikelyInvariantGenerator, Parameter


class DummyProgram(InstrumentedProgram):

    def __init__(self, constants):
        InstrumentedProgram.__init__(self, None, (int, ), LikelyInvariantGenerator([Parameter(0, "x", int)]), 5, dict())

    def run_program_on_input(self, input, info_collector):
        return (1, 0, 0, 0, 1), (0.5, 0.5, 1, 1, 0)

class TestInstrumentedProgram(TestCase):
    def test(self):
        ip = DummyProgram(dict())

        print(ip.get_coverage((1,)))
        print(ip.get_coverage((1,)))

        print(ip.get_objectivefunc((1,)))

        print(ip.get_inputs_for_coverage((1, 0, 0, 0, 1)))
        print(ip.get_inputs_for_coverage((1, 0, 1, 0, 1)))
