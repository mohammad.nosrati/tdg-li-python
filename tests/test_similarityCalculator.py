import inspect
import operator
from unittest import TestCase

from common.InfoCollector import InfoCollector
from common.Instrumenter import Instrumenter
from common.SimilarityCalculator import SimilarityCalculator

import math

from common.sample_programs import triangle


def dot_product2(v1, v2):
    return sum(map(operator.mul, v1, v2))


def vector_cos5(v1, v2):
    prod = dot_product2(v1, v2)
    len1 = math.sqrt(dot_product2(v1, v1))
    len2 = math.sqrt(dot_product2(v2, v2))
    return prod / (len1 * len2)


class TestSimilarityCalculator(TestCase):
    def test_sim_cos(self):
        x = (0, 0, 0, 0, 1, 0, 0, 0, 0, 0)
        y = (0.0, 0.0, 0.0, 0.61633, 0.0, 0.0, 0, 0, 0, 0)
        cos = SimilarityCalculator().sim_cos(x, y)
        print(cos)
        self.assertEqual(cos, 0, "not equal")

    def test_sim_cos_2(self):

        x = (0.847535, 0.0, 0.0, 0.0, 0.0, 0.0, 0, 0, 0, 0)
        y = (1, 0, 0, 0, 0, 0, 0, 0, 0, 0)

        cos = SimilarityCalculator().sim_cos(x, y)
        print(cos)
        self.assertEqual(cos, 1, "not equal")

    def test_instrument_triangle(self):
        # tri = Instrumenter().instrument(triangle, (int, int, int))
        # tdg = TDGAGeneticsWithLI(met_inst, goals)

        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(triangle))

        exec(compile(tree, filename="<ast>", mode="exec"))  # redefine triangle()

        info_collector = InfoCollector(branch_count)

        exec("triangle(3, 3, 3, info_collector)")
        target = info_collector.coverage
        target_fit_vec = info_collector.fitness_vec
        print("target: ", target)

        info_collector = InfoCollector(branch_count)
        exec("triangle(1, 2, 3, info_collector)")
        print("coverage: ", info_collector.coverage)
        print("fitness_vec (this is not against a particular goal): ", info_collector.fitness_vec)

        print("similarity (1,2,3): ", SimilarityCalculator().sim_path(target, info_collector.coverage, info_collector.fitness_vec))
        print("similarity (3,3,3): ", SimilarityCalculator().sim_path(target, target, target_fit_vec))
