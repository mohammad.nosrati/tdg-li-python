import ast
import inspect
from unittest import TestCase

from termcolor import colored

from common.InfoCollector import InfoCollector
from common.Instrumenter import Instrumenter, prettydump
from common.sample_programs import triangle, mid, tcas, dayOfWeek


def t():
    return True


def one():
    return 1


def f():
    res = 0
    if t():
        res += 1
    if one() + 2 == 3:
        res += 2
    if 4 - 2 == 3 and (7 > 1000):
        res += 4
    if 3 + 2 == 5 and (5 < 4 or 4 > 3):
        res += 3


class TestInstrumenter(TestCase):
    def test_instrument_program(self):
        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(triangle))

        exec(compile(tree, filename="<ast>", mode="exec"))  # redefine triangle()

        info_collector = InfoCollector(branch_count)
        exec("triangle(3, 3, 3, info_collector)")
        print("coverage: ", info_collector.coverage)
        print("fitness: ", info_collector.fitness_vec)
        self.assertEqual((0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0), tuple(info_collector.coverage))

        info_collector = InfoCollector(branch_count)
        exec("triangle(3, 3, -1, info_collector)")
        print("coverage: ", info_collector.coverage)
        print("fitness: ", info_collector.fitness_vec)
        self.assertEqual((1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0), tuple(info_collector.coverage))

        info_collector = InfoCollector(branch_count)
        exec("triangle(1, 5, 5, info_collector)")
        print("coverage: ", info_collector.coverage)
        print("fitness: ", info_collector.fitness_vec)
        self.assertEqual((0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1), tuple(info_collector.coverage))

    def test_instrument_mid(self):
        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(mid))

        exec(compile(tree, filename="<ast>", mode="exec"))  # redefine triangle()

        info_collector = InfoCollector(branch_count)
        exec("mid(3, 4, 5, info_collector)")

        print("coverage: ", info_collector.coverage)

        self.assertEqual((1, 1, 0, 0, 0), tuple(info_collector.coverage))

    def test_instrument_f(self):
        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(f))

        exec(compile(tree, filename="<ast>", mode="exec"))  # redefine triangle()

        info_collector = InfoCollector(branch_count)
        exec("f(info_collector)")

        print("coverage: ", info_collector.coverage)

        self.assertEqual((1, 1, 0, 1), tuple(info_collector.coverage))

    def test_dayofweek(self):
        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(dayOfWeek))

        exec(compile(tree, filename="<ast>", mode="exec"))

        info_collector = InfoCollector(branch_count)
        exec("dayOfWeek(1, 1, 2017, info_collector)")

        print("coverage: ", info_collector.coverage)

        # self.assertEqual((1, 1, 0, 0, 0), tuple(info_collector.coverage))

    def test_tcas(self):
        instrumenter = Instrumenter()
        tree, branch_count = instrumenter.instrument_program(inspect.getsource(tcas))

        exec(compile(tree, filename="<ast>", mode="exec"))

        info_collector = InfoCollector(branch_count)
        # exec("tcas(601, True, True, 1, 1, 1, 1, 1, 1, 0, 1, True, info_collector)")
        exec("tcas(2683134048, True, False, -2585122351, -3322476844, 2730189443, -2883773823, -145064337, -2782785870, 1807097934, -1866675354, False, info_collector)")


        print("coverage: ", info_collector.coverage)

        # def test_instrument(self):
        #     ins = Instrumenter().instrument(triangle, (int, int, int))
        #     collector = InfoCollector(ins.branch_count)
        #     ins.run_program_on_input((3, 3, 3), collector)
        #
        #     print(collector)
        #     print(ins)


    def test_learn_how_to_convert_to_ast(self):
        source = """def func(z):
            print('here')
            x,y=1,4
            info_collector = InfoCollector(coverage = [0] * (branch_count + 1))
            if info_collector.evaluate_condition(None, 'or', None, 0, 0, [x is None, x == y]):
                info_collector.coverage[1] = 10
            if x == 1:
                 print('llll')
            # if evaluate_condition(1, '==', 2,1, True, 10):
            #     print('jj')
            if 12 <= 15:
                print(1)
            elif 4 > x + y:
                print(3)
            else:
                print(2)

            if 2 > 3:
                print('jjjj')
            else:
                print('jjjjjd')
        """

        tree = ast.parse(source)  # parse an ast tree from the source code
        # MyTransformer().visit(tree)
        # exec(compile(tree, filename="<ast>", mode="exec"))  # redefine triangle()
        # print('===========')

        for node in ast.walk(tree):
            for child in ast.iter_child_nodes(node):
                child.parent = node

        print(prettydump(tree))

    def test_ast_children(self):
        source = """def func(z):
            print('here')
            x,y=1,4
            if x > 3:
                info_collector.coverage[1] = 10
            else:
                 print('llll')
            print('2')
        """

        tree = ast.parse(source)

        for node in ast.walk(tree):
            print(colored(node, 'red'))
            for child in ast.iter_child_nodes(node):
                print(child)

        #
        # for node in ast.walk(tree):
        #     for child in ast.iter_child_nodes(node):
        #         child.parent = node