from unittest import TestCase

from common.likelyinvariant import *


class TestLikelyInvariantGenerator(TestCase):
    def test_check_for_invs(self):

        lig = LikelyInvariantGenerator([Parameter(0, "a", int), Parameter(1, "b", int), Parameter(2, "c", int)])

        lig.check_for_invs(0, [1,2,3])
        lig.check_for_invs(0, [2,2,2])
        lig.check_for_invs(0, [-1,2,2])
        lig.check_for_invs(0, [-1,-1,2])

        print(lig.invariants_by_branch[0])

