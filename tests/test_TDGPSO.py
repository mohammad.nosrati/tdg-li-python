from unittest import TestCase

from algorithms.TDGPSO import TDGPSO
from common.Instrumenter import Instrumenter
from common.sample_programs import triangle


class TestTDGPSO(TestCase):
    def test_initial_population(self):
        method, args = triangle, (int, int, int)
        met_inst = Instrumenter().instrument(method, args)
        b = met_inst.branch_count
        goals = list()
        for i in range(b):
            br = [0] * b
            br[i] = 1
            goals.append(tuple(br))

        pso = TDGPSO(met_inst, goals[2])
        pso.MAX_ITERATION = 10000
        pso.MININT = -10000
        pso.MAXINT = 10000

        pso.initial_population()

        def print_status():
            print("Pop: ", pso.population)
            print("g: ", pso.gbest)
            print("b: ", pso.pbest)
            print("V: ", pso.V)

        pso.cover_goals()


        print("best: ", pso.gbest)