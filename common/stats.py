from scipy.stats import stats
import numpy as np

alpha = 0.05

def compare_with_alpha(stat, pvalue, labelx, labely, verbose=True):
    if pvalue < alpha:
        if verbose:
            print(f"H0 (mu_{labelx:10} = mu_{labely:10}) - rejected.           {stat:.4f}, {pvalue}")
        return True
    else:
        if verbose:
            print(f"H0 (mu_{labelx:10} = mu_{labely:10}) - cannot be rejected. {stat:.4f}, {pvalue}")
        return False

def mann_whitney_test(x, y, labelx, labely):
    m_d = means(x, y, labelx, labely)

    U, p = stats.mannwhitneyu(x, y, alternative='less')

    h0_rejected = compare_with_alpha(U, p, labelx, labely)
    return h0_rejected, m_d, p


def ttest(x, y, labelx, labely):
    m_d = means(x, y, labelx, labely)

    # null hypothesis that 2 independent samples have identical average (expected) values.
    tstat, p = stats.ttest_ind(x, y, equal_var=False)

    h0_rejected = compare_with_alpha(tstat, p, labelx, labely)
    return h0_rejected, m_d, p



def anovatest(x, y, labelx, labely, verbose=True):
    m_d = means(x, y, labelx, labely, verbose)

    # null hypothesis: two or more groups have the same population mean.
    stat, p = stats.f_oneway(x, y)

    h0_rejected = compare_with_alpha(stat, p, labelx, labely, verbose=verbose)

    return h0_rejected, m_d, p



def means(x, y, labelx, labely, verbose=True):
    m_x = np.mean(x)
    m_y = np.mean(y)
    if verbose:
        print(f"{labelx}: {m_x}, {labely}: {m_y}")

    return m_y - m_x
