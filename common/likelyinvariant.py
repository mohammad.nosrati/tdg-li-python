import itertools

LI_CONSTANTS = {0, 1}
LI_OPERATORS = ["==", "!=", "<", "<=", ">", ">="]

class Constant:
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return str(self.value)

    def __hash__(self):
        return hash(self.value)

    def __eq__(self, other):
        if not isinstance(other, Constant):
            return False
        return self.value == other.value


class Parameter:
    def __init__(self, index, name, type):
        self.index = index
        self.name = name
        self.type = type

    def __repr__(self):
        return self.name

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, other):
        if not isinstance(other, Parameter):
            return False
        return self.name == other.name


class LikelyInvariant:
    def __init__(self, operands, operator):  # TODO: must improve a lot
        self.operands = list(operands)    # deep copy: Paramters or constants
        self.operator = operator

    def satisfies(self, input_vector):
        values = []

        for i, oprnd in enumerate(self.operands):
            if type(oprnd) == Parameter:
                values.append(input_vector[oprnd.index])
            elif type(oprnd) == Constant:
                values.append(oprnd.value)

        if self.operator == '==':
            return values[0] == values[1]
        elif self.operator == '!=':
            return values[0] != values[1]
        elif self.operator == '<':
            return values[0] < values[1]
        elif self.operator == '<=':
            return values[0] <= values[1]
        elif self.operator == '>':
            return values[0] > values[1]
        elif self.operator == '>=':
            return values[0] >= values[1]

    def copy(self):
        return LikelyInvariant(self.operands, self.operator)

    def __repr__(self):
        return str(self.operands[0]) + self.operator + str(self.operands[1])    # TODO could be made more general

    def __hash__(self):
        return hash((tuple(self.operands), self.operator))

    def __eq__(self, o):
        return (tuple(self.operands), self.operator) == (tuple(o.operands), o.operator)


class LikelyInvariantGenerator:
    def __init__(self, paramters):
        self.invariants_by_branch = dict()
        self.input_size = len(paramters)
        self.inv_set = set()
        self.parameters = paramters

        self.constants = set(LI_CONSTANTS)
        self.OPERATORS = list(LI_OPERATORS)

    def add_constants(self, consts):
        self.constants.update(consts)

    def check_for_invs(self, branch, input_vector):
        if self.invariants_by_branch.get(branch) is None:
            self.invariants_by_branch[branch] = self.init_likely_invariants()

        removed_items = set()
        for inv in self.invariants_by_branch[branch]:
            if not self.check_for_inv(inv, input_vector):
                removed_items.add(inv)

        self.invariants_by_branch[branch] -= removed_items

    def init_likely_invariants(self):
        likely_invariants = set()
        for size in range(1, 3):  # take two parameters together #range(1, len(self.parameters) + 1):
            params = set(itertools.combinations(self.parameters, size))  # set of tuples, like {("a", "b"), ("a, "c"), ...}

            if size == 1:
                for param in params:
                    if param[0].type == bool:
                        for c in [True, False]:
                            likely_invariants.add(LikelyInvariant([param[0], Constant(c)], '=='))
                    elif param[0].type == int:
                        for op in self.OPERATORS:
                            for c in self.constants:
                                likely_invariants.add(LikelyInvariant([param[0], Constant(c)], op))
            elif size == 2:
                for param in params:
                    paramlist = list(param)

                    if paramlist[0].type == paramlist[1].type:
                        if paramlist[0].type == int:
                                for op in self.OPERATORS:
                                    likely_invariants.add(LikelyInvariant([param[0], param[1]], op))
                        elif paramlist[0].type == bool:
                            likely_invariants.add(LikelyInvariant([param[0], param[1]], '=='))
                            likely_invariants.add(LikelyInvariant([param[0], param[1]], '!='))
        return likely_invariants

    def check_for_inv(self, inv, input_vector):
        return inv.satisfies(input_vector)

    def __repr__(self):
        return str(self.invariants_by_branch)