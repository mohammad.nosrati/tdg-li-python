from termcolor import colored

# from _pyexz3.symbolic.constraint import Constraint
# from _pyexz3.symbolic.predicate import Predicate
# from _pyexz3.symbolic.symbolic_types import SymbolicInteger
# from _pyexz3.symbolic.symbolic_types.symbolic_type import SymbolicObject
# from symbolic.MyEngine import MyEngine


class InfoCollector:
    def __init__(self, branches_count):
        self.coverage = [0] * branches_count
        self.fitness_vec = [0] * branches_count

        self.fitness_old_style = True

        self.C = 10**10

        self.constraints = [list()] * branches_count

        self.current_branch = None

    def eval_cond(self, cond_tree):
        K = 1

        left = right = None

        if type(cond_tree) != list:
            bd = K # it was None before
            if type(cond_tree) == bool:  # TODO not yet sure - Boolean: if true return 0 else K
                bd = 0 if cond_tree else K
            return cond_tree, bd

        op = cond_tree[0]
        if op != 'or' and op != 'and':
            if len(cond_tree) > 1:
                left = cond_tree[1]
            if len(cond_tree) > 2:
                right = cond_tree[2]
        else:
            # each c in cond_tree[1:] (rest of the list) is something like ['<', 0, 5].
            # ri is the result of ith condition, oi is its branch distance (objective function i think)
            # map is applied to cond_tree[1:]. for each c, when eval_cond is applied to it, it turns to a pair of True/False (1 or 0) and BD
            pairs = list(map(lambda c: self.eval_cond(c), cond_tree[1:]))  # [(r1, o1), (r2, o2), ...]  , ri: each condition (lambda part), oi: each branch distance
            pred_list, bds = zip(*pairs)  # [r1, r2, ...], [o1, o2, ...] (convert list of pairs to two lists)

        # if isinstance(left, SymbolicInteger):
        #     left = left.getConcrValue()
        # if isinstance(right, SymbolicInteger):
        #     right = right.getConcrValue()

        if op == '==':
            result = left == right
        elif op == '!=':
            result = left != right
        elif op == '>':
            result = left > right
        elif op == '>=':
            result = left >= right
        elif op == '<':
            result = left < right
        elif op == '<=':
            result = left <= right
        elif op == 'and':
            result = all(pred_list)
        elif op == 'or':
            result = any(pred_list)
        else:
            print("UNKNOWN OPERATOR")
            raise NotImplementedError

        ## bd:

        # I should use concrete values here instead of the actual sym object (otherwise extra (and wrong) calculations will be recorded for symbolic variables

        # if isinstance(left, SymbolicInteger):
        #     left = left.getConcrValue()
        # if isinstance(right, SymbolicInteger):
        #     right = right.getConcrValue()

        r = result
        # if isinstance(result, SymbolicInteger):
        #     r = result.getConcrValue()


        if r:
            branch_dist = 0
        else:
            if op == '==':
                branch_dist = abs(left - right) + K
            elif op == '!=':
                branch_dist = K
            elif op == '>':
                branch_dist = right - left + K
            elif op == '>=':
                branch_dist = right - left + K
            elif op == '<':
                branch_dist = left - right + K
            elif op == '<=':
                branch_dist = left - right + K
            elif op == 'and':
                branch_dist = sum(bds)
            elif op == 'or':
                branch_dist = min(bds)

        return result, branch_dist

    def evaluate_condition(self, condition_tree, branch, else_branch):
        result, bd = self.eval_cond(condition_tree)

        if else_branch != -1:
            else_result = not result
            # print("else_branch ", else_branch, ':', else_result)
            # cov[else_branch] = 1 if else_result else 0

        # print("condition_tree: ", condition_tree)

        # print("branch ", branch, ':', result)

        # another similarity formula: 1 / (1 + bd), not working as well
        self.fitness_vec[branch] = 1 - max(0, min(1, bd / self.C))
        # TODO else_branch

        # TODO: DEL
        # print(colored("DEBUG- branch:", 'green'), branch, SymbolicObject.SI)

        # p = Predicate(MyEngine.current_symbolic_object, result)
        # cons = MyEngine.constraints_by_branch.get(branch)
        # if cons is None:
        #     cons = list()
        # MyEngine.constraints_by_branch[branch] = cons
        #
        # cons.append(p)
        #############


        # if result:
        #     MyEngine.current_branch = branch
        #     # MyEngine.current_path_constraint.add(MyEngine.current_predicate, branch)
        # elif else_branch != -1:
        #     MyEngine.current_branch = else_branch
            # MyEngine.current_path_constraint.add(MyEngine.current_predicate, else_branch)

            # if MyEngine.current_symbolic_object is not None:
        #     MyEngine.process(result, MyEngine.current_symbolic_object)
        # self.constraints[branch].append(Constraint(None, SymbolicObject.SI.current_constraint))#Constraint(None, SymbolicObject.SI)

        # MyEngine.add_constraint_to_current_branch()  # This is when we are sure we are inside 'branch'. We may change this

        return result

    def branch_covered(self, branch):
        self.current_branch = branch
        self.coverage[branch] = 1
        # MyEngine.current_branch = branch
        # MyEngine.add_constraint_to_current_branch()  # This is when we are sure we are inside 'branch'. We may change this

    def __repr__(self):
        return str(("coverage: ", self.coverage, "fitness: ", self.fitness_vec))