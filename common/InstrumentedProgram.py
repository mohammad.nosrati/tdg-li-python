import collections
import inspect
import random
from abc import abstractmethod, ABC
import math
from queue import Queue

from termcolor import colored
from z3 import unsat

# from _pyexz3.symbolic.explore import ExplorationEngine
# from _pyexz3.symbolic.invocation import FunctionInvocation
# from _pyexz3.symbolic.path_to_constraint import PathToConstraint
# from _pyexz3.symbolic.predicate import Predicate
#
# from _pyexz3.symbolic.symbolic_types.symbolic_type import SymbolicObject
from algorithms.TestDataGenerationAlgorithm import Individual
from common.InfoCollector import InfoCollector
# from symbolic.MyEngine import MyEngine
# from symbolic.PathConstraint import PathConstraint
# from symbolic.Z3Solver import Z3Solver
# from _pyexz3.symbolic.symbolic_types import SymbolicInteger

class InstrumentedProgram(ABC):

    def __init__(self, target_method, input_types, lig, branch_count, constants, verbose=True):
        self.coverage_by_inputs = dict()
        self.objfunc_by_inputs = dict()
        self.inputs_by_coverage = dict()

        self.inputs_by_branch_coverage = dict()
        self.name = target_method.__name__

        self.input_types = input_types
        self.lig = lig
        self.parameters = self.lig.parameters
        self.target_method = target_method
        self.branch_count = branch_count

        self.best_obj_for_branch = dict.fromkeys(range(self.branch_count), 0)
        self.new_best_input_for_branch = dict()#dict.fromkeys(range(self.branch_count), None)

        self.constants = constants
        self.args = inspect.getfullargspec(target_method).args
        # add 3 random constants from program source:
        # self.lig.add_constants(random.sample([x for x in self.constants.get("numeric", set()) if type(x) == int], 3))



        if verbose:
            print("CONSTANTS: ", self.constants)

        self.constraints_to_solve = []
        # self.sym_args = []

        # self.symbolic = True
        # if self.symbolic:
            # inv = self.symbolic_init()
            # engine = ExplorationEngine(inv, solver="z3")
        # for i, t in enumerate(self.input_types):
        #     if t is int:
        #         self.sym_args.append(SymbolicInteger(self.args[i], 0))



            # generatedInputs, returnVals, path = engine.explore(20)

            # print("generated inputs:", generatedInputs)
            # print("return vals:", returnVals)
            # print("path:", path.root_constraint, "current_constraint", path.current_constraint)
            # print("result:", result)

    def get_input_types(self): # tuple of int, str, ...
        return self.input_types

    def get_coverage(self, input, _addinputinfo=True):
        if not isinstance(input, tuple):
            if isinstance(input, Individual):
                input = input.prog_input
            else:
                input = tuple(input)

        if self.coverage_by_inputs.get(input) is not None:
            return self.coverage_by_inputs[input]

        info_collector = InfoCollector(self.branch_count)
        self.run_program_on_input(input, info_collector)
        coverage, objf = info_collector.coverage, info_collector.fitness_vec

        for i, v in enumerate(coverage):
            if v == 1:
                self.lig.check_for_invs(i, input)

        if _addinputinfo:
            self.add_input_info(input, coverage, objf)
        return coverage

    def get_objectivefunc(self, input):
        if not isinstance(input, tuple):
            input = tuple(input)

        if self.objfunc_by_inputs.get(input) is not None:
            return self.objfunc_by_inputs[input]

        info_collector = InfoCollector(self.branch_count)
        self.run_program_on_input(input)
        coverage, objf = info_collector.coverage, info_collector.fitness_vec

        for i, v in enumerate(coverage, info_collector):
            if v == 1:
                self.lig.check_for_invs(i, input)

        self.add_input_info(input, coverage, objf)
        return objf

    def add_input(self, input):
        if not isinstance(input, tuple):
            input = tuple(input)

        if self.coverage_by_inputs.get(input) is None:
            info_collector = InfoCollector(self.branch_count)
            self.run_program_on_input(input, info_collector)
            coverage, objf = info_collector.coverage, info_collector.fitness_vec

            for i, v in enumerate(coverage):
                if v == 1:
                    self.lig.check_for_invs(i, input)

            self.coverage_by_inputs[input] = coverage
            self.objfunc_by_inputs[input] = objf

            self.add_to_inputs_by_coverage(coverage, input)

            self.check_objf_upgrade(input)

    def check_objf_upgrade(self, input):
        objf = self.objfunc_by_inputs[input]
        for b in range(self.branch_count):
            if self.best_obj_for_branch[b] < objf[b]:
                print(colored(f'UPGRADE for branch {b}: {objf[b]} ({self.best_obj_for_branch[b]}), by: {input}', 'magenta'))
                self.best_obj_for_branch[b] = objf[b]
                self.new_best_input_for_branch[b] = input

    def reset_best_new_input_for_branch(self):
        self.new_best_input_for_branch = dict()#dict.fromkeys(range(self.branch_count), None)

    def add_to_inputs_by_coverage(self, coverage, input):
        if not isinstance(input, tuple):
            input = tuple(input)

        for i, inp in enumerate(input):
            if type(inp) != self.get_input_types()[i]:
                raise Exception("error in type: " + str(inp) + ". type(inp) = " + type(inp) + ", type(prog_inp) = " + self.get_input_types()[i])

        if not isinstance(coverage, tuple):
            coverage = tuple(coverage)

        inputs = self.inputs_by_coverage.get(coverage)
        if inputs is None:
            #inputs = list() # TODO: list or set or deque?
            inputs = collections.deque(maxlen=1000)
            self.inputs_by_coverage[coverage] = inputs
        inputs.append(input)

        # branch coverage:
        for b, covered in enumerate(coverage):
            if covered:
                inputs_for_b = self.inputs_by_branch_coverage.get(b)
                if inputs_for_b is None:
                    inputs_for_b = set()
                    self.inputs_by_branch_coverage[b] = inputs_for_b
                inputs_for_b.add(input)


    def add_input_info(self, input, coverage, objf):
        if not isinstance(input, tuple):
            input = tuple(input)

        self.coverage_by_inputs[input] = coverage
        self.objfunc_by_inputs[input] = objf

        self.add_to_inputs_by_coverage(coverage, input)

    def get_inputs_for_coverage(self, coverage):
        return self.inputs_by_coverage.get(coverage)

    def get_inputs_for_branch_coverage(self, b):
        return self.inputs_by_branch_coverage.get(b)

    def run_program_on_input(self, input, info_collector):
        try: # run the target method (which is a function):
            self.target_method(*list(input), info_collector)    # unpack input tuple (x0, x1, ...) for target_method: program(x0, x1, ..., info_collector)
        except IndexError as ex:
            pass
        except:
            pass
            # print(input, ex)
            # exit(1)




    # def symbolic_init(self):
    #     inv = FunctionInvocation(self._execute, self._resetCallback)
    #     argspec = inspect.getargspec(self.target_method)
    #     for a in argspec.args:
    #         if not a in inv.getNames() and a != 'info_collector':
    #             self._initializeArgumentSymbolic(inv, a, 0, SymbolicInteger)
    #
    #     return inv
    #
    # def _initializeArgumentSymbolic(self, inv, f, val, st):
    #     inv.addArgumentConstructor(f, val, lambda n, v: st(n, v))
    #
    # def _execute(self, **args):
    #     return self.target_method(**args)
    #
    # def _resetCallback(self):
    #     pass

    def get_branch_coverage_percent(self):
        tot_covered = 0
        for b in range(self.branch_count):
            if self.get_inputs_for_branch_coverage(b) is not None and\
                    len(self.get_inputs_for_branch_coverage(b)) > 0:
                tot_covered += 1

        return tot_covered / self.branch_count

    # def next_sym_input(self):
    #     if len(MyEngine.all_constraints) == 0:
    #         return tuple(self.default_sym_input())
    #
    #
    #
    #     cs = [c for c in MyEngine.all_constraints if c.solution is None]
    #     if not cs:
    #         print("nothing else")
    #         return None
    #     c = cs[0]
    #     print(colored(c, 'blue'))
    #
    #
    #     z3solver = Z3Solver()
    #     c.solution = z3solver.solve_and_create_input(c, self.input_types, self.args)
    #     if not c.solution or c.solution == unsat:
    #         c.solution = self.default_sym_input()
    #
    #     return c.solution


    # def default_sym_input(self):
    #     sym_input = []
    #     for i, t in enumerate(self.input_types):
    #         if t is int:
    #             sym_int = SymbolicInteger(self.args[i], 0)
    #
    #             sym_input.append(sym_int)
    #     return tuple(sym_input)

    # def sym_with_init_val(self, val):
    #     sym_input = []
    #     for i, t in enumerate(self.input_types):
    #         if t is int:
    #             sym_int = SymbolicInteger(self.args[i], val[i])
    #
    #             sym_input.append(sym_int)
    #     return tuple(sym_input)

    # def add_sym_input(self, sym_input):
    #     self.add_input(MyEngine.get_concrete_input(sym_input))

    # def symbolic_run_old(self):
    #     info_collector = InfoCollector(self.branch_count)
    #
    #     sym_input = []
    #
    #     # TODO: DEL
    #     SymbolicObject.SI = PathToConstraint(lambda c: self.addConstraint(c))
    #     ######
    #
    #     for i, t in enumerate(self.input_types):
    #         if t is int:
    #             sym_int = SymbolicInteger(self.args[i], 0)
    #
    #             sym_input.append(sym_int)
    #
    #     MyEngine.current_path_constraint = PathConstraint()
    #
    #     MyEngine.current_solution = tuple(sym_input)
    #     self.run_program_on_input(MyEngine.current_solution, info_collector)
    #     coverage, objf = info_collector.coverage, info_collector.fitness_vec
    #
    #     # TODO: DEL or orgnize
    #     print(coverage, objf)
    #
    #     print(sym_input)
    #
    #     # print(colored(MyEngine.current_path_constraint, 'red'))
    #     #########
    #
    #     # print(SymbolicObject.SI)
    #
    #     #print(self.constraints_to_solve)
    #
    #     # print(info_collector.constraints)
    #     # for b in range(len(info_collector.constraints)):
    #     #     print(colored(str(b), 'green'), info_collector.constraints[b])

    def addConstraint(self, constraint):
        self.constraints_to_solve.append(constraint)
        # make sure to remember the input that led to this constraint
        #constraint.inputs = self._getInputs()

    # def symbolic_run(self, max_its=50, start_value=None):
    #     info_collector = InfoCollector(self.branch_count)
    #
    #     its = 0
    #
    #     MyEngine.current_method = self
    #     if start_value is None:
    #         MyEngine.add_solution(self.default_sym_input())
    #     else:
    #         MyEngine.add_solution(self.sym_with_init_val(start_value))
    #
    #     while not MyEngine.solution_queue.empty() and self.get_branch_coverage_percent() != 1 and its < max_its:
    #         its += 1
    #
    #         # TODO must be deleted or organized
    #         MyEngine.current_path_constraint = PathConstraint()
    #
    #         # MyEngine.current_solution = self.next_sym_input()
    #
    #         # if not MyEngine.solution_queue.empty():
    #         #     MyEngine.current_solution = MyEngine.solution_queue.get()
    #         # else:
    #         #     continue
    #         MyEngine.current_solution = MyEngine.solution_queue.get(block=False)
    #
    #         print(colored(MyEngine.current_solution, 'blue', attrs=['bold']))
    #         if MyEngine.current_solution is None:
    #             continue
    #         MyEngine.current_constraint = None
    #         self.run_program_on_input(MyEngine.current_solution, info_collector)
    #         coverage, objf = info_collector.coverage, info_collector.fitness_vec
    #
    #         self.add_sym_input(MyEngine.current_solution)
    #
    #         print(colored('coverage: ' + str(self.get_branch_coverage_percent()) + '%', 'magenta'))
    #
    #         # for c in MyEngine.all_constraints:
    #         #     print(colored(str(c), 'green'))
    #
    #     print('iterations:', its)
    #     self.print_solutions()
    #     # print(colored(MyEngine.solution_queue.queue, 'green'))

    def print_solutions(self):
        for b in range(self.branch_count):
            print(colored(b, 'green'), colored(str(self.get_inputs_for_branch_coverage(b)), 'blue'))