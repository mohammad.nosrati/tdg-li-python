import math
import random

# from misc.archive.topologicalsort import Graph

# rand_min = -30000
# rand_max = 30000

# rand_min = -100000
# rand_max = 100000

rand_min = -2 ** 20
rand_max = 2 ** 20

max_interval = abs(rand_max - rand_min)


class Range:
    def __init__(self, minval, maxval):
        self.minval = minval
        self.maxval = maxval

    def intersection(self, r):
        if self.minval > r.maxval or self.maxval < r.minval:
            return None
        l = max(self.minval, r.minval)
        u = min(self.maxval, r.maxval)
        return Range(l, u)

    def __repr__(self):
        return "[" + str(self.minval) + ", " + str(self.maxval) + "]"


def get_invariants_for_cov(lig, cov):
    total_inv = ""

    clauses = set()

    for i in range(len(cov)):
        if cov[i] == 1:
            for inv in lig.invariants_by_branch[i]:
                clauses.add(inv)

    first = True
    for c in clauses:
        total_inv += (" and " if not first else "") + str(c)
        first = False

    if total_inv == "":
        total_inv = "NOTHING"
    return total_inv, clauses


def findValuesFor(clauses, n):
    gin = [0] * n
    det = [False] * n

    if len(clauses) == 0:
        return None
    for c in sorted(clauses, key=lambda cl: cl.num_of_variables()):
        if c.num_of_variables() == 1:
            if not det[c.operands[0]]:
                if c.operator == '>= 0':
                    gin[c.operands[0]] = random.randint(0, 10000)
                elif c.operator == '> 0':
                    gin[c.operands[0]] = random.randint(1, 10000)
                elif c.operator == '<= 0':
                    gin[c.operands[0]] = random.randint(-10000, 0)
                elif c.operator == '< 0':
                    gin[c.operands[0]] = random.randint(-10000, -1)
            det[c.operands[0]] = True

        elif c.num_of_variables() == 2:
            if c.operator == '==':
                gin[c.operands[1]] = gin[c.operands[0]]
            elif c.operator == '!=':
                if gin[c.operands[0] == gin[c.operands[1]]]:
                    # print("!=, do something")
                    pass  # TODO
            elif c.operator == '>':
                if gin[c.operands[0]] > gin[c.operands[1]]:
                    pass

    # print(gin)
    return gin


def select_unassigned_variable(deps, det, clauses, n):
    vars_ = {v for v in range(0, n) if not det[v]}
    if not vars_:
        return None
    # return random.choice(tuple(vars))
    for v in deps:
        if v in vars_:
            return v


def select_val_for_var(var, assignments, det, clauses):
    clauses_for_var_only = {cl for cl in clauses if var in cl.operands and cl.num_of_remaining_vars(det) == 1}

    if not clauses_for_var_only:
        assignments[var] = random.randint(-rand_min, rand_max)
        return True, assignments  # TODO: what should i do here. just select a random value?

    # clauses_for_var_sorted = sorted(clauses_for_var_only, key = lambda cl : cl.assignment_priority())



    ranges = list()
    # TODO: problem: a < b & b < c
    for cl in clauses_for_var_only:
        other = None
        if cl.num_of_variables() > 1:  # TODO: NOT SURE
            if cl.operands[0] == var:
                other = cl.operands[1]
            else:
                other = cl.operands[0]

        if cl.operator == '==':
            # ranges.append(set(range(assignments[other], assignments[other] + 1)))
            ranges.append(Range(assignments[other], assignments[other]))
        elif cl.operator == '>= 0':
            # ranges.append(set(range(0, 10001)))
            ranges.append(Range(0, rand_max))
        elif cl.operator == '> 0':
            # ranges.append(set(range(1, 10001)))
            ranges.append(Range(1, rand_max))
        elif cl.operator == '<= 0':
            # ranges.append(set(range(-10000, 1)))
            ranges.append(Range(rand_min, 0))
        elif cl.operator == '< 0':
            # ranges.append(set(range(-10000, 0)))
            ranges.append(Range(rand_max, -1))
        elif cl.operator == '>':
            # ranges.append(set(range(assignments[other] + 1, 10001)))
            ranges.append(Range(assignments[other] + 1, rand_max))
        elif cl.operator == '>=':
            # ranges.append(set(range(assignments[other] + 1, 10001)))
            ranges.append(Range(assignments[other], rand_max))
        elif cl.operator == '<':
            # ranges.append(set(range(-10000, assignments[other])))
            ranges.append(Range(rand_min, assignments[other] - 1))
        elif cl.operator == '<=':
            # ranges.append(set(range(-10000, assignments[other] + 1)))
            ranges.append(Range(rand_min, assignments[other]))
        elif cl.operator == '!=':
            # TODO
            pass

    result_range = Range(rand_min, rand_max)
    for r in ranges:
        result_range = result_range.intersection(r)
        if result_range is None:
            break

    if not result_range:
        return False, assignments
    else:
        assignments[var] = random.randint(result_range.minval, result_range.maxval)
        return True, assignments


def get_dependencies_order(clauses, n):
    all_eqs = {cl for cl in clauses if cl.operator == '=='}
    all_lts = {cl for cl in clauses if cl.operator == '<' or cl.operator == '<='}
    all_gts = {cl for cl in clauses if cl.operator == '>' or cl.operator == '>='}

    # graph = [[0 for i in range(n)] for j in range(n)]
    g = Graph(n)

    for eq in all_eqs:
        g.addEdge(eq.operands[0], eq.operands[1])
        g.addEdge(eq.operands[1], eq.operands[0])
        # graph[eq.operands[0]][eq.operands[1]] = 1
        # graph[eq.operands[1]][eq.operands[0]] = 1

    for lt in all_lts:
        g.addEdge(lt.operands[0], lt.operands[1])
        # graph[lt.operands[0]][lt.operands[1]] = 1

    for gt in all_lts:
        g.addEdge(gt.operands[1], gt.operands[0])
        # graph[gt.operands[1]][gt.operands[0]] = 1

    return g.topologicalSort()


def backtrack(assignments, det, clauses, n):
    deps = get_dependencies_order(clauses, n)
    var = select_unassigned_variable(deps, det, clauses, n)

    # tries = 0
    vars_ = list()
    vars_.append(var)

    # TODO: backtracking
    while var is not None:  # and tries < 100:    # TODO i can save tried values
        success, assign = select_val_for_var(var, assignments, det, clauses)

        # if not success:
        #     tries += 1
        # else:
        if success:
            assignments = assign
            det[var] = True

            all_eqs = {cl for cl in clauses if cl.operator == '==' and (cl.operands[0] == var or cl.operands[1] == var)}
            if all_eqs:  # TODO: this is to handle == sooner than other vars. maybe we can incorporate this in topological sort
                for eq in all_eqs:
                    if eq.operands[0] == var:
                        assignments[eq.operands[1]] = assignments[var]
                        det[eq.operands[1]] = True
                    else:
                        assignments[eq.operands[0]] = assignments[var]
                        det[eq.operands[0]] = True

        var = select_unassigned_variable(deps, det, clauses, n)
        # backtrack(assignments, det, clauses, n)
    return True, assignments


def backtracking_search(clauses, n):
    assignments = [0] * n
    det = [False] * n

    success, assignments = backtrack(assignments, det, clauses, n)
    if success:
        return assignments
    else:
        return None


def cov_triangle(a, b, c):
    cov = [0] * 10

    if a <= 0 or b <= 0 or c <= 0:
        cov[0] = 1
        return cov
    ##            return Type.INVALID;
    trian = 0
    if a == b:
        cov[1] = 1
        trian = trian + 1
    if a == c:
        cov[2] = 1
        trian = trian + 2

    if b == a:  # inserted bug: should be b == c
        cov[3] = 1
        trian = trian + 3
    if trian == 0:
        if a + b <= c or a + c <= b or b + c <= a:
            cov[4] = 1
            return cov
            # return Type.INVALID;
        else:
            cov[5] = 1
            return cov
            # return Type.SCALENE;
    if trian > 3:
        cov[6] = 1
        return cov
        # return Type.EQUILATERAL;
    if trian == 1 and a + b > c:
        cov[7] = 1
        return cov
        # return Type.ISOSCELES;
    elif trian == 2 and a + c > b:
        cov[8] = 1
        return cov
        # return Type.ISOSCELES;
    elif trian == 3 and b + c > a:
        cov[9] = 1
        # return Type.ISOSCELES;
    return cov
    # return Type.INVALID;


def cov_triangle_li(a, b, c, lig):
    cov = [0] * 10
    inp_vec = (a, b, c)

    if a <= 0 or b <= 0 or c <= 0:
        cov[0] = 1
        lig.check_for_invs(0, inp_vec)
        return cov
    ##            return Type.INVALID;
    trian = 0
    if a == b:
        cov[1] = 1
        lig.check_for_invs(1, inp_vec)
        trian = trian + 1
    if a == c:
        cov[2] = 1
        lig.check_for_invs(2, inp_vec)
        trian = trian + 2

    if b == a:  # inserted bug: should be b == c
        cov[3] = 1
        lig.check_for_invs(3, inp_vec)
        trian = trian + 3
    if trian == 0:
        if a + b <= c or a + c <= b or b + c <= a:
            lig.check_for_invs(4, inp_vec)
            cov[4] = 1
            return cov
            # return Type.INVALID;
        else:
            cov[5] = 1
            lig.check_for_invs(5, inp_vec)
            return cov
            # return Type.SCALENE;
    if trian > 3:
        cov[6] = 1
        lig.check_for_invs(6, inp_vec)
        return cov
        # return Type.EQUILATERAL;
    if trian == 1 and a + b > c:
        lig.check_for_invs(7, inp_vec)
        cov[7] = 1
        return cov
        # return Type.ISOSCELES;
    elif trian == 2 and a + c > b:
        lig.check_for_invs(8, inp_vec)
        cov[8] = 1
        return cov
        # return Type.ISOSCELES;
    elif trian == 3 and b + c > a:
        lig.check_for_invs(9, inp_vec)
        cov[9] = 1
        # return Type.ISOSCELES;
    return cov
    # return Type.INVALID;


def cov_triangle_li_obj(a, b, c, lig):
    cov = [0] * 10
    inp_vec = (a, b, c)
    obj_func = [0] * 10

    obj_func[0] = 1 - (min(a / max_interval, b / max_interval, c / max_interval))
    if a <= 0 or b <= 0 or c <= 0:
        cov[0] = 1
        lig.check_for_invs(0, inp_vec)

        return cov, obj_func, "INVALID"
    ##            return Type.INVALID;
    trian = 0

    obj_func[1] = 1 - abs(a - b) / max_interval
    if a == b:
        cov[1] = 1
        lig.check_for_invs(1, inp_vec)

        trian = trian + 1

    obj_func[2] = 1 - abs(a - c) / max_interval
    if a == c:
        cov[2] = 1
        lig.check_for_invs(2, inp_vec)
        trian = trian + 2

    obj_func[3] = 1 - abs(a - b) / max_interval
    if b == a:  # inserted bug: should be b == c
        cov[3] = 1
        lig.check_for_invs(3, inp_vec)
        trian = trian + 3

    if trian == 0:
        # or: min(z(c1), z(c2))
        obj_func[4] = 1 - (
        min(abs(a + b - c) / max_interval, abs(a + c - b) / max_interval, abs(b + c - a) / max_interval))
        obj_func[5] = 1 - (
        abs(c - a - b) / max_interval + abs(b - a - c) / max_interval + abs(a - b - c) / max_interval)
        if a + b <= c or a + c <= b or b + c <= a:
            lig.check_for_invs(4, inp_vec)
            cov[4] = 1
            return cov, obj_func, "INVALID"
            # return Type.INVALID;
        else:
            cov[5] = 1
            lig.check_for_invs(5, inp_vec)
            return cov, obj_func, "SCALENE"
            # return Type.SCALENE;

    obj_func[6] = 1 - (3 - trian) / max_interval
    if trian > 3:
        cov[6] = 1
        lig.check_for_invs(6, inp_vec)
        return cov, obj_func, "EQUILATERAL"
        # return Type.EQUILATERAL;

    obj_func[7] = 1 - (abs(trian - 1) / max_interval + (c - (a + b)) / max_interval)
    obj_func[8] = 1 - (abs(trian - 2) / max_interval + (b - (a + c)) / max_interval)
    obj_func[9] = 1 - (abs(trian - 3) / max_interval + (b - (b + c)) / max_interval)
    if trian == 1 and a + b > c:
        lig.check_for_invs(7, inp_vec)
        cov[7] = 1
        return cov, obj_func, "ISOSCELES"
        # return Type.ISOSCELES;

    elif trian == 2 and a + c > b:
        lig.check_for_invs(8, inp_vec)
        cov[8] = 1
        return cov, obj_func, "ISOSCELES"
        # return Type.ISOSCELES;
    elif trian == 3 and b + c > a:
        lig.check_for_invs(9, inp_vec)
        cov[9] = 1
        return cov, obj_func, "ISOSCELES"
        # return Type.ISOSCELES;
    return cov, obj_func, "INVALID"
    # return Type.INVALID;


def oracle_triangle(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        return "INVALID"
    trian = 0
    if a == b:
        trian = trian + 1
    if a == c:
        trian = trian + 2
    if b == c:
        trian = trian + 3
    if trian == 0:
        if a + b <= c or a + c <= b or b + c <= a:
            return "INVALID"
        else:
            return "SCALENE"
    if trian > 3:
        return "EQUILATERAL"
    if trian == 1 and a + b > c:
        return "ISOSCELES"
    elif trian == 2 and a + c > b:
        return "ISOSCELES"
    elif trian == 3 and b + c > a:
        return "ISOSCELES"
    return "INVALID"


def sim_smc(a, b):
    c00 = c01 = c10 = c11 = 0

    for i in range(len(a)):
        if a[i] == 0 and b[i] == 0:
            c00 += 1
        elif a[i] == 0 and b[i] == 1:
            c01 += 1
        elif a[i] == 1 and b[i] == 0:
            c10 += 1
        else:
            c11 += 1

    s = (c00 + c11) / (c00 + c01 + c10 + c11)
    return s


def sim_jaccard(a, b):
    c00 = c01 = c10 = c11 = 0

    for i in range(len(a)):
        if a[i] == 0 and b[i] == 0:
            c00 += 1
        elif a[i] == 0 and b[i] == 1:
            c01 += 1
        elif a[i] == 1 and b[i] == 0:
            c10 += 1
        else:
            c11 += 1

    s = c11 / (c01 + c10 + c11)
    return s


def sim_cos(a, b):
    aDotb = 0
    aNorm = 0
    bNorm = 0

    for i in range(len(a)):
        aDotb += a[i] * b[i]
        aNorm += a[i] * a[i]
        bNorm += b[i] * b[i]
    aNorm = math.sqrt(aNorm)
    bNorm = math.sqrt(bNorm)

    if aNorm == 0 or bNorm == 0:
        return 0
    return aDotb / (aNorm * bNorm)


def sim_path(target, cov, obj_vec):
    if tuple(target) == tuple(cov):
        return 1

    for i in range(len(obj_vec)):
        obj_vec[i] *= target[i]
        if target[i] == 1:
            if cov[i] == 1:
                obj_vec[i] = 1
            else:
                obj_vec[i] -= 0.1
                if obj_vec[i] < 0:
                    obj_vec[i] = 0
    return sim_cos(target, obj_vec)


# def mid(x, y, z, lig):
#     cov = [0] * 6
#     inp_vec = (x, y, z)
#     obj_func = [0] * 6
#
#     m = z
#
#     obj_func[0] = 1 - (y - z) / max_interval
#     obj_func[3] = 1 - (z - y) / max_interval
#     if y < z:
#         cov[0] = 1
#         lig.check_for_invs(1, inp_vec)
#
#         obj_func[1] = 1 - (x - y) / max_interval
#         obj_func[2] = 1 - ((x - y) / max_interval + (x - z) / max_interval)
#
#         if x < y:
#             lig.check_for_invs(1, inp_vec)
#             cov[1] = 1
#             m = y
#         elif x < z:
#             lig.check_for_invs(2, inp_vec)
#             cov[2] = 1
#             m = y
#     else:
#         cov[3] = 1
#         lig.check_for_invs(3, inp_vec)
#
#         obj_func[4] = 1 - (y - x) / max_interval
#         obj_func[5] = 1 - ((x - y) / max_interval + (z - x) / max_interval)
#         if x > y:
#             lig.check_for_invs(4, inp_vec)
#             cov[4] = 1
#             m = y
#         elif x > z:
#             lig.check_for_invs(5, inp_vec)
#             cov[5] = 1
#             m = x
#
#     return cov, obj_func


class Color:
    reset = '\033[0m'
    bold = '\033[01m'
    disable = '\033[02m'
    underline = '\033[04m'
    reverse = '\033[07m'
    strikethrough = '\033[09m'
    invisible = '\033[08m'

    black = '\033[30m'
    red = '\033[31m'
    green = '\033[32m'
    orange = '\033[33m'
    blue = '\033[34m'
    purple = '\033[35m'
    cyan = '\033[36m'
    lightgrey = '\033[37m'
    darkgrey = '\033[90m'
    lightred = '\033[91m'
    lightgreen = '\033[92m'
    yellow = '\033[93m'
    lightblue = '\033[94m'
    pink = '\033[95m'
    lightcyan = '\033[96m'
