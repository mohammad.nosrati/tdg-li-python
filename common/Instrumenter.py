import ast
import inspect
from ast import AST, iter_fields

from astor import to_source
from termcolor import colored

from common.InstrumentedProgram import InstrumentedProgram
from common.likelyinvariant import LikelyInvariantGenerator, Parameter


class Instrumenter:

    def instrument_from_tree(self, tree, input_types, name, args, verbose=True):
        constant_collector = ConstantCollector()
        constant_collector.visit(tree)

        # linenoSetter = OriginalLineNoSetter()
        # linenoSetter.visit(tree)
        #
        # pp = LinenoPrinter()
        # pp.visit(tree)

        transformer = InstrumentTransformer()
        transformer.visit(tree)

        # tree = ast.fix_missing_locations(tree)

        # pp = LinenoPrinter()
        # pp.visit(tree)

        # xyz = NodeToLine()
        # xyz.visit(tree)

        # bp = BranchPrinter()
        # bp.visit(tree)

        # print(colored(xyz.line_by_node, 'blue'))



        if verbose:
            print(to_source(tree))
            print(transformer.graph)

        namespace = {}
        exec(compile(tree, filename="<ast>", mode="exec"), namespace)

        inst_method = namespace[name]

        params = [Parameter(i, name, input_types[i]) for (i, name) in enumerate(args)]
        lig = LikelyInvariantGenerator(params)

        # print(lig.input_size, lig.operand_names)

        instrumented_program = InstrumentedProgram(inst_method, input_types, lig, transformer.branch_count,
                                      constant_collector.constants, False)

        # instrumented_program.line_map = linenoSetter.my_map

        return instrumented_program

    def instrument(self, target_method, input_types, verbose=True):
        source = inspect.getsource(target_method)

        tree = ast.parse(source)

        name = target_method.__name__
        args = inspect.getfullargspec(target_method).args

        return self.instrument_from_tree(tree, input_types, name, args, verbose)

    def instrument_program(self, source):
        tree = ast.parse(source)
        transformer = InstrumentTransformer()
        transformer.visit(tree)

        print(to_source(tree))
        return tree, transformer.branch_count
        # TODO return Subclass of InstrumentedProgram


class ConstantCollector(ast.NodeTransformer):
    def __init__(self):
        self.constants = dict()
        self.constants["numeric"] = set()
        self.constants["string"] = set()

    def visit_Num(self, node):
        self.constants["numeric"].add(node.n)
        return node

    def visit_Str(self, node):
        self.constants["string"].add(node.s)
        return node


# class OriginalLineNoSetter(ast.NodeTransformer):
#
#
#     def __init__(self) -> None:
#         self.my_map = dict()
#
#     def generic_visit(self, node):
#         if hasattr(node, "lineno"):
#             node.original_lineno = node.lineno
#             node.original_col_offset = node.col_offset
#
#             self.my_map[(node.lineno, node.col_offset)] = node
#             # print(node.lineno, node.col_offset)
#
#         return super().generic_visit(node)

# class LinenoPrinter(ast.NodeTransformer):
#     def generic_visit(self, node):
#         print("-----------")
#         print(prettydump(node))
#         if hasattr(node, "lineno"):
#             print(node)
#             print(node.lineno, node.col_offset)
#             if hasattr(node, "original_lineno"):
#                 print(node.original_lineno, node.original_col_offset)
#         return super().generic_visit(node)


class NodeToLine(ast.NodeTransformer):

    def __init__(self) -> None:
        self.line_by_node = dict()

    def generic_visit(self, node):
        if hasattr(node, "lineno"):
            self.line_by_node[node] = node.lineno
        return super().generic_visit(node)

class BranchSetter(ast.NodeTransformer):
    def __init__(self, root, branch) -> None:
        self.branch = branch
        self.root = root

    def generic_visit(self, node):
        if node != self.root:
            node.branch = self.branch
        return super().generic_visit(node)

class BranchPrinter(ast.NodeVisitor):
    def generic_visit(self, node):
        if hasattr(node, "branch"):
            try:
                print("---------")
                print(prettydump(node))
                print(to_source(node), colored(node.branch, 'red'))
            except AttributeError as e:
                print("ae: " + str(e))
        return super().generic_visit(node)


class InstrumentTransformer(ast.NodeTransformer):
    def __init__(self):
        self.mapper = {
            ast.Eq: "==",
            ast.Gt: ">",
            ast.Lt: "<",
            ast.LtE: "<=",
            ast.GtE: ">=",
            ast.NotEq: "!="
        }

        self.branch_count = 0
        self.current_branch = -1
        self.current_else_branch = -1
        self.is_test = False  # are we now in a test of if/for/while?

        self.graph = dict()

    def visit_FunctionDef(self, node):
        #### graph:
        for child in ast.iter_child_nodes(node):
            child.parent = -1

            # child.branch = -1
        ####
        self.set_branch_of_all_child_nodes(node, -1)

        node.args.args.append(ast.arg(arg='info_collector', annotation=None))
        # node.args.args.append(ast.arg(arg='coverage', annotation=None))
        # node.args.args.append(ast.arg(arg='evaluate_condition', annotation=None))
        ast.fix_missing_locations(node.args)
        self.generic_visit(node)
        return node

    def set_branch_of_all_child_nodes(self, node, b):
        bs = BranchSetter(node, b)
        bs.visit(node)

    def instrument_branch_covered(self, node, has_else=False):
        # Old code: info_collector.coverage[b] = 1 instead of info_collector.branch_covered(b)
        # cov_code = ast.Assign(targets=[
        #     ast.Subscript(value=
        #                   ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()), attr='coverage', ctx=ast.Load()),
        #                   slice=ast.Index(value=ast.Num(n=self.current_branch)),
        #                   ctx=ast.Store()),
        # ], value=ast.Num(n=1))

        cov_code = ast.Expr(ast.Call(func=ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()),
                                                        attr='branch_covered', ctx=ast.Load()),
                                     args=[ast.Num(n=self.current_branch)],
                                     keywords=[]))
        node.body.insert(0, cov_code)

        # for child in ast.iter_child_nodes(node):
        #     child.branch = self.current_branch
        self.set_branch_of_all_child_nodes(node, self.current_branch)

        if has_else:
            # Old code:
            # cov_code = ast.Assign(targets=[
            #     ast.Subscript(value=ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()),
            #                                       attr='coverage', ctx=ast.Load()),
            #                   slice=ast.Index(value=ast.Num(n=self.current_else_branch)), ctx=ast.Store()),
            # ], value=ast.Num(n=1))

            cov_code = ast.Expr(ast.Call(func=ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()),
                                                            attr='branch_covered', ctx=ast.Load()),
                                         args=[ast.Num(n=self.current_else_branch)],
                                         keywords=[]))
            node.orelse.insert(0, cov_code)

            # for child in node.orelse:
            #     child.branch = self.current_else_branch
            self.set_branch_of_all_child_nodes(node, self.current_branch)

        ast.fix_missing_locations(node)


    def visit_While(self, node):
        self.current_branch = self.branch_count

        #### graph:
        for child in ast.iter_child_nodes(node):
            child.parent = self.current_branch
        self.graph[self.current_branch] = node.parent
        #### graph:

        self.branch_count += 1

        self.instrument_branch_covered(node)

        new_test = ast.Call(func=ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()),
                                               attr='evaluate_condition', ctx=ast.Load()),
                            args=[node.test,
                                  ast.Num(n=self.current_branch),
                                  ast.Num(n=-1),
                                  ], keywords=[])
        node.test = new_test

        # only add evaluate_condition for if/for/while/ tests (and not all bool ops)
        self.is_test = True
        self.generic_visit(node.test)
        self.is_test = False

        self.generic_visit(node)
        ast.fix_missing_locations(node)
        return node

    def visit_For(self, node):
        self.current_branch = self.branch_count

        #### graph:
        for child in ast.iter_child_nodes(node):
            child.parent = self.current_branch
        self.graph[self.current_branch] = node.parent
        ####

        self.branch_count += 1

        self.instrument_branch_covered(node)

        # only add evaluate_condition for if/for/while/ tests (and not all bool ops)
        # self.is_test = True
        # self.generic_visit(node.test)
        # self.is_test = False

        self.generic_visit(node)
        ast.fix_missing_locations(node)
        return node

    def visit_If(self, node):
        ############# <coverage
        self.current_branch = self.branch_count

        #### graph:
        for child in ast.iter_child_nodes(node):
            child.parent = self.current_branch
        self.graph[self.current_branch] = node.parent
        ####

        self.branch_count += 1

        self.current_else_branch = -1  # explicitly find if this has an else

        # check if this has an "else":
        if len(node.orelse) > 0 and \
                not isinstance(node.orelse[0], ast.If):  # this is for preventing destroying "elif"s

            self.current_else_branch = self.branch_count

            #### graph:
            for n in node.orelse:
                n.parent = self.current_else_branch
            self.graph[self.current_else_branch] = node.parent
            #####

            self.branch_count += 1

        #################
        #### graph:
        # not sure about this code:
        # i'm trying to set parent of all if ... elif ... elif ... else nodes one thing: parent of the main if
        elseif = node.orelse
        while len(elseif) > 0 and isinstance(elseif[0], ast.If):
            # elseif[0].branch = node.parent.branch
            # self.graph[elseif[0].branch] = node.parent.branch
            elseif[0].parent = node.parent
            elseif = elseif[0].orelse
        ###################

        self.instrument_branch_covered(node, self.current_else_branch != -1)

        ################ /coverage>

        new_test = ast.Call(func=ast.Attribute(value=ast.Name(id='info_collector', ctx=ast.Load()),
                                               attr='evaluate_condition', ctx=ast.Load()),
                            args=[node.test,
                                  ast.Num(n=self.current_branch),
                                  ast.Num(n=self.current_else_branch),
                                  ], keywords=[])
        node.test = new_test

        # only add evaluate_condition for if/for/while/ tests (and not all bool ops)
        self.is_test = True
        self.generic_visit(node.test)
        self.is_test = False

        self.generic_visit(node)
        ast.fix_missing_locations(node)
        return node

    def visit_Compare(self, node):
        if not self.is_test:
            return node

        if len(node.ops) == 1:
            op = self.mapper.get(node.ops[0].__class__)

            # convert a < b to ["<", a, b]
            cond_as_list = ast.List(elts=[ast.Str(s=op), node.left, node.comparators[0]], ctx=ast.Load())

            node = cond_as_list
            self.generic_visit(node)

            return node

    def visit_BoolOp(self, node):
        if not self.is_test:
            return node

        if type(node.op) == ast.And:
            op = "and"
        else:
            op = "or"

        cond_list = [ast.Str(op)]
        for v in node.values:
            cond_list.append(v)

        # convert a < b or b < c to ["or", ["<", a, b], ["<", b, c]]
        cond_as_list = ast.List(elts=cond_list, ctx=ast.Load())

        node = cond_as_list
        self.generic_visit(node)

        return node


def prettydump(node, annotate_fields=True, include_attributes=False, indent='  '):
    """
    Return a formatted dump of the tree in *node*.  This is mainly useful for
    debugging purposes.  The returned string will show the names and the values
    for fields.  This makes the code impossible to evaluate, so if evaluation is
    wanted *annotate_fields* must be set to False.  Attributes such as line
    numbers and column offsets are not dumped by default.  If this is wanted,
    *include_attributes* can be set to True.
    """

    def _format(node, level=0):
        if isinstance(node, AST):
            fields = [(a, _format(b, level)) for a, b in iter_fields(node)]
            rv = '%s(%s' % (node.__class__.__name__, ', '.join(
                ('%s=%s' % field for field in fields)
                if annotate_fields else
                (b for a, b in fields)
            ))
            if include_attributes and node._attributes:
                rv += fields and ', ' or ' '
                rv += ', '.join('%s=%s' % (a, _format(getattr(node, a)))
                                for a in node._attributes)
            return rv + ')'
        elif isinstance(node, list):
            lines = ['[']
            lines.extend((indent * (level + 2) + _format(x, level + 2) + ','
                          for x in node))
            if len(lines) > 1:
                lines.append(indent * (level + 1) + ']')
            else:
                lines[-1] += ']'
            return '\n'.join(lines)
        return repr(node)

    if not isinstance(node, AST):
        raise TypeError('expected AST, got %r' % node.__class__.__name__)
    return _format(node)


# def get_node_branch(tree_node):
#     if tree_node.hasattr("branch"):
#         return tree_node.branch
#     else