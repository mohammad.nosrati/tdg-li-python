import os
from itertools import cycle
import numpy as np

from matplotlib import pyplot as plt

def diagram_path():
    dpath = os.path.join(os.path.dirname(__file__), "..\output")
    return dpath

def generate_COV_IT_vectors(itcounts_by_run, names, percents_by_run):
    RUN = []  # seeds:    [0, 1, ...., 9]
    COV = {}  # percents: {'ga:single': [10, 20, 10, 90, ..., 100],
    #             'ga-li':    [100, 100, ..........., 100], ...}
    IT = {}
    for n in names:
        COV[n] = []
        IT[n] = []
    for seed, percent in percents_by_run.items():
        RUN.append(seed)

        for n in names:
            if n not in percents_by_run[seed]:
                COV[n].append(np.NaN)
                IT[n].append(np.NaN)
            else:
                COV[n].append(percents_by_run[seed][n])
                IT[n].append(min(50, itcounts_by_run[seed][n])) # for max only 50

    return RUN, COV, IT


def plot_graphs(methodname, percents_by_run, names, itcounts_by_run, onlyshow, subfolder=""):
    RUN, COV, IT = generate_COV_IT_vectors(itcounts_by_run, names, percents_by_run)# plot_cov_and_it_graphs(COV, IT, RUN, methodname, onlyshow)
    ################

    outpath = diagram_path()
    if subfolder != "":
        outpath = os.path.join(outpath, subfolder)

    ###### box
    plt.clf()
    fig_avg_cov = plt.figure("coverage avg")
    plt.title(methodname)
    labels, data = [*zip(*COV.items())]
    plt.boxplot(data)
    plt.xticks(range(1, len(labels) + 1), labels, rotation=30)
    if onlyshow:
        plt.show()
    else:
        fig_avg_cov.savefig(os.path.join(outpath, "avg_cov_" + methodname.lower() + ".pdf"))

    ###############
    plt.clf()
    fig_avg_it =  plt.figure("iteration avg")

    plt.title(methodname)
    labels, data = [*zip(*IT.items())]
    plt.boxplot(data)
    plt.xticks(range(1, len(labels) + 1), labels, rotation=30)
    if onlyshow:
        plt.show()
    else:
        fig_avg_it.savefig(os.path.join(outpath, "avg_it_" + methodname.lower() + ".pdf"))


    # from scipy import stats
    #
    # F_cov, p_cov = stats.f_oneway(*Y.values())
    # F_it, p_it = stats.f_oneway(*Y.values())
    #
    # print(methodname, " - Cov: F: ", F_cov, "p: ", p_cov, " it: F: ",F_it, "p: ", p_it)


def plot_cov_and_it_graphs(COV, IT, RUN, methodname, onlyshow):
    styles = ['-', '--', '--', '-.', '-.', ':', ':', ':']
    markers = ['o', '^', 'v', '<', '>', '|', '+', '*']
    ls_cycler = cycle(styles)
    marker_cycler = cycle(markers)

    outpath = diagram_path()

    ############# coverages:
    plt.clf()
    # plt.rc('axes', prop_cycle=(cycler(color=['r', 'g', 'b', 'y']) +
    #                            cycler(linestyle=['-', '--', ':', '-.'])))
    fig_cov = plt.figure("coverages")
    for i, mode in enumerate(COV.keys()):
        plt.plot(RUN, COV[mode], marker=next(marker_cycler), label=mode, ls=next(ls_cycler))
        # plt.plot(X, Y[mode])
    plt.title(methodname)
    plt.legend()
    plt.xlabel("run")
    # plt.xticks([s + 0.3 for s in range(len(X))], [s for s in range(len(X))])  # second param: labels
    plt.xticks([s for s in range(len(RUN))])
    plt.ylabel("coverage")
    if onlyshow:
        plt.show()
    else:
        fig_cov.savefig(outpath + "/" + methodname.lower() + "_coverages.pdf")

    ############# iterations:
    ls_cycler = cycle(styles)
    marker_cycler = cycle(markers)
    plt.clf()
    fig_it = plt.figure("iterations")
    for i, mode in enumerate(COV.keys()):
        plt.plot(RUN, IT[mode], marker=next(marker_cycler), label=mode, ls=next(ls_cycler))
        # plt.plot(X, Z[mode])
    plt.title(methodname)
    plt.legend()
    plt.xlabel("run")
    # plt.xticks([s + 0.3 for s in range(len(X))], [s for s in range(len(X))])  # second param: labels
    plt.xticks([s for s in range(len(RUN))])
    plt.ylabel("iterations")
    if onlyshow:
        plt.show()
    else:
        fig_it.savefig(outpath + "/" + methodname.lower() + "_iterations.pdf")

