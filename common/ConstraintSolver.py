from z3 import *

from common.commons import Color
from .likelyinvariant import *


class ConstraintSolver:
    def __init__(self):
        self.solver = Solver()
        self.solver.set("timeout", 300)



        # self.BITSCOUNT = 32
        # self.MAXINT = 2 ** self.BITSCOUNT - 1
        # self.MININT = -2 ** self.BITSCOUNT + 1

        # set_option('auto_config', False)
        # set_option('smt.phase_selection', 5)
        # set_option('smt.random_seed', 10)

    def solve_constraint(self, constraint, params, already_found, verbose=False):
        for i, p in enumerate(params):
            if p == int or p is None:
                exec("param_%d = Int('param_%d')" % (i, i))
                # exec("param_%d = BitVecInt('param_%d', 5)" % (i, i))
            if p == bool:
                exec("param_%d = Bool('param_%d')" % (i, i))
        if constraint == '':
            if verbose:
                print(Color.bold, Color.red, "Empty Constraint", Color.reset)
            return


        self.solver.reset()
        self.solver.add(eval(constraint))
        self.solver.push()

        # exclude already found values:
        for t in already_found:
            if len(t) == 1:
                v = "Not(param_0 == %s)" % t[0]
            else:
                v = "Not(And("
                for j, x in enumerate(t):
                    if j > 0:
                        v += ", "
                    v += "param_%d == %s" % (j,x)
                v += "))"
            try:
                self.solver.add(eval(v))
            except Exception as ex:
                print(ex)
                return None


        if verbose:
            print(Color.cyan, v, Color.reset)

        # solve
        result = self.solver.check()

        if result != sat:
            if verbose:
                print("No solution")
            return None

        m = self.solver.model()
        if verbose:
            print(self.solver)

        vals = []
        for i, p in enumerate(params):
            if params[i] is int:
                num = m[eval('param_%d' % i)]
                n = num.as_long() if num is not None else 0
                # if n > self.MAXINT:
                #     n = self.MAXINT
                # elif n < self.MININT:
                #     n = self.MININT
                vals += [n]  # TODO 0 or None?
            elif params[i] is bool:
                b = m[eval('param_%d' % i)]
                vals += [bool(b) if b is not None else False]

        return tuple(vals)

    @staticmethod
    def make_constraint(inv):
        constraints = []

        # iterate on clauses in the likely inv:
        for cl in inv:
            operands = []

            for opn in cl.operands:
                if type(opn) == Parameter:
                    operands.append("param_%d" % opn.index)
                elif type(opn) == Constant:
                    operands.append(str(opn.value))

            if isinstance(cl.operands[0].type, bool):
                if type(cl.operands[1]) == Constant and not isinstance(cl.operands[1].value, bool):
                    if operands[1].value == 0:
                        operands[1] = False
                    else:
                        operands[1] = True
                if cl.operator not in ["==", '!=']:
                    cl.operator = "!="
            constraints += [operands[0] + " " + cl.operator + " " + operands[1]]
            #
            # if cl.operator == '==':
            #     constraints += ["param_%d == param_%d" % (cl.operands[0].index, cl.operands[1].index)]
            # elif cl.operator == '>':
            #     constraints += ["param_%d > param_%d" % (cl.operands[0].index, cl.operands[1].index)]
            # elif cl.operator == '>=':
            #     constraints += ["param_%d >= param_%d" % (cl.operands[0].index, cl.operands[1].index)]
            # elif cl.operator == '<':
            #     constraints += ["param_%d < param_%d" % (cl.operands[0].index, cl.operands[1].index)]
            # elif cl.operator == '<=':
            #     constraints += ["param_%d <= param_%d" % (cl.operands[0].index, cl.operands[1].index)]
            # elif cl.operator == '!=':
            #     constraints += ["param_%d != param_%d" % (cl.operands[0].index, cl.operands[1].index)]

        return ", ".join(constraints)