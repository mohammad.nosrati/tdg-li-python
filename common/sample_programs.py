from math import floor

import math

from termcolor import colored


def triangle(a, b, c):
    if a <= 0 or b <= 0 or c <= 0:
        return "INVALID"
    trian = 0
    if a == b:
        trian = trian + 1
    if a == c:
        trian = trian + 2
    if b == c:
        trian = trian + 3
    if trian == 0:
        if a + b <= c or a + c <= b or b + c <= a:
            return "INVALID"
        else:
            return "SCALENE"
    if trian > 3:
        return "EQUILATERAL"
    if trian == 1 and a + b > c:
        return "ISOSCELES"
    elif trian == 2 and a + c > b:
        return "ISOSCELES"
    elif trian == 3 and b + c > a:
        return "ISOSCELES"
    return "INVALID"


def mid(x, y, z):
    m = z
    if y < z:
        if x < y:
            m = y
        elif x < z:
            m = y
    else:
        if x > y:
            m = y
        elif x > z:
            m = x

    return m


def gcd(a, b):
    if a < 0:
        a = -a

    if b < 0:
        b = -b

    if a == 0 or b == 0:
        print("The input values must be greater than zero")
        return 0

    while b > 0:
        tmp = a % b
        a = b
        b = tmp
    return a


def dayOfWeek(day, month, year):
    import math  # TODO import
    JGREG = 15 + 31 * (10 + 12 * 1582)
    # HALFSECOND = 0.5
    julianYear = year
    if year < 0:
        julianYear = julianYear + 1

    if year < 1 or year > 10000 or month < 1 or month > 12 or day < 1 or day > 31:
        return "Invalid"

    julianMonth = month
    if month > 2:
        julianMonth = julianMonth + 1
    else:
        julianYear = julianYear - 1
        julianMonth = julianMonth + 13

    t = math.floor(365.25 * julianYear)
    s = math.floor(30.6001 * julianMonth)
    julian = t + s + day + 1720995.0
    temp = day + 31 * (month + 12 * year)
    if temp >= JGREG:
        # change over to Gregorian calendar
        ja = int(0.01 * julianYear)
        julian = julian + 2 - ja + (0.25 * ja)

    jul = math.floor(julian)

    jul = (jul + 1) % 7

    if jul == 0:
        return "Sunday"
    elif jul == 1:
        return "Monday"
    elif jul == 2:
        return "Tuesday"
    elif jul == 3:
        return "Wednesday"
    elif jul == 4:
        return "Thursday"
    elif jul == 5:
        return "Friday"
    elif jul == 6:
        return "Saturday"


def numOfDays(month1, day1, month2, day2, year):
    # //***********************************************************
    # // Calculate the number of Days between the two given days in
    # // the same year.
    # // preconditions : day1 and day2 must be in same year
    # // 1 <= month1, month2 <= 12
    # // 1 <= day1, day2 <= 31
    # // month1 <= month2
    # // The range for year: 1 ... 10000
    # //***********************************************************

    if year < 1 or year > 10000 or month1 < 1 or month1 > 12 or day1 < 1 or \
            day1 > 31 or month2 < 1 or month2 > 12 or day2 < 1 or day2 > 31:
        return "Invalid"

    if month2 == month1:  # // in the same month
        numDays = day2 - day1
    else:
        # // Skip month 0.
        daysIn = [0, 31, 0, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        # // Are we in a leap year?
        m4 = year % 4
        m100 = year % 100
        m400 = year % 400

        if (m4 != 0) or ((m100 == 0) and (m400 != 0)):
            daysIn[2] = 28
        else:
            daysIn[2] = 29
        # // start with days in the two months
        numDays = day2 + (daysIn[month1] - day1)  ## IndexError will occur if month1 is not in [1,12]
        # // add the days in the intervening months
        for i in range(month1 + 1, month2):
            numDays = daysIn[i] + numDays

    return numDays


class TCAS:
    OLEV = 600  # / * in feets / minute * /
    MAXALTDIFF = 600  # / * max altitude difference in feet * /
    MINSEP = 300  # / * min separation in feet * /
    NOZCROSS = 100  # / * in feet * /

    NO_INTENT = 0
    DO_NOT_CLIMB = 1
    DO_NOT_DESCEND = 2

    TCAS_TA = 1
    OTHER = 2

    UNRESOLVED = 0
    UPWARD_RA = 1
    DOWNWARD_RA = 2

    def __init__(self, cvs, hc, ttrv, ota, otar, otTa, alv, upS, dS, oRAC, oc, ci):
        self.Cur_Vertical_Sep = cvs
        self.High_Confidence = hc
        self.Two_of_Three_Reports_Valid = ttrv
        self.Own_Tracked_Alt = ota
        self.Own_Tracked_Alt_Rate = otar
        self.Other_Tracked_Alt = otTa
        self.Alt_Layer_Value = alv  # / * 0, 1, 2, 3 * /
        self.Up_Separation = upS
        self.Down_Separation = dS

        # / * state variables * /
        self.Other_RAC = oRAC  # / * NO_INTENT, DO_NOT_CLIMB, DO_NOT_DESCEND * /
        self.Other_Capability = oc  # / * TCAS_TA, OTHER * /
        self.Climb_Inhibit = ci  # / * true / false * /

        self.Positive_RA_Alt_Thresh = [None, None, None, None]  # = new int[4];
        self.initialize()

    def initialize(self):
        self.Positive_RA_Alt_Thresh[0] = 400
        self.Positive_RA_Alt_Thresh[1] = 500
        self.Positive_RA_Alt_Thresh[2] = 640
        self.Positive_RA_Alt_Thresh[3] = 740

    def ALIM(self):
        return self.Positive_RA_Alt_Thresh[self.Alt_Layer_Value]

    def Inhibit_Biased_Climb(self):
        return self.Up_Separation + self.NOZCROSS if self.Climb_Inhibit else self.Up_Separation

    def Non_Crossing_Biased_Climb(self):
        upward_preferred = self.Inhibit_Biased_Climb() > self.Down_Separation
        if upward_preferred:
            result = not (self.Own_Below_Threat()) or (
                    (self.Own_Below_Threat()) and (not (self.Down_Separation >= self.ALIM())))
        else:
            result = self.Own_Above_Threat() and (self.Cur_Vertical_Sep >= self.MINSEP) and (
                    self.Up_Separation >= self.ALIM())

        return result

    def Non_Crossing_Biased_Descend(self):
        upward_preferred = self.Inhibit_Biased_Climb() > self.Down_Separation
        if upward_preferred:
            result = self.Own_Below_Threat() and (self.Cur_Vertical_Sep >= self.MINSEP) and (
                    self.Down_Separation >= self.ALIM())
        else:
            result = not (self.Own_Above_Threat()) or (
                    (self.Own_Above_Threat()) and (self.Up_Separation >= self.ALIM()))
        return result

    def Own_Below_Threat(self):
        return self.Own_Tracked_Alt < self.Other_Tracked_Alt

    def Own_Above_Threat(self):
        return self.Other_Tracked_Alt < self.Own_Tracked_Alt

    def alt_sep_test(self):
        enabled = self.High_Confidence and (self.Own_Tracked_Alt_Rate <= self.OLEV) and (
                self.Cur_Vertical_Sep > self.MAXALTDIFF)
        tcas_equipped = self.Other_Capability == self.TCAS_TA
        intent_not_known = self.Two_of_Three_Reports_Valid and self.Other_RAC == self.NO_INTENT

        alt_sep = self.UNRESOLVED

        if enabled and ((tcas_equipped and intent_not_known) or not tcas_equipped):
            need_upward_RA = self.Non_Crossing_Biased_Climb() and self.Own_Below_Threat()
            need_downward_RA = self.Non_Crossing_Biased_Descend() and self.Own_Above_Threat()
            if need_upward_RA and need_downward_RA:
                alt_sep = self.UNRESOLVED
            elif need_upward_RA:
                alt_sep = self.UPWARD_RA
            elif need_downward_RA:
                alt_sep = self.DOWNWARD_RA
            else:
                alt_sep = self.UNRESOLVED

        return alt_sep


# print(TCAS(1, True, True, 1, 1, 1, 1, 1, 1, 1, 1, True).alt_sep_test())
# print(TCAS(627, False, False, 621,  216, 382, 1, 400,641, 1, 1, False).alt_sep_test())


def tcas(cvs, hc, ttrv, ota, otar, otTa, alv, upS, dS, oRAC, oc, ci):
    OLEV = 600  # / * in feets / minute * /
    MAXALTDIFF = 600  # / * max altitude difference in feet * /
    MINSEP = 300  # / * min separation in feet * /
    NOZCROSS = 100  # / * in feet * /

    NO_INTENT = 0
    DO_NOT_CLIMB = 1
    DO_NOT_DESCEND = 2

    TCAS_TA = 1
    OTHER = 2

    UNRESOLVED = 0
    UPWARD_RA = 1
    DOWNWARD_RA = 2

    Cur_Vertical_Sep = cvs
    High_Confidence = hc
    Two_of_Three_Reports_Valid = ttrv
    Own_Tracked_Alt = ota
    Own_Tracked_Alt_Rate = otar
    Other_Tracked_Alt = otTa
    Alt_Layer_Value = alv  # / * 0, 1, 2, 3 * /        # what to do for alv < 0 or alv > 3?
    Up_Separation = upS
    Down_Separation = dS

    # / * state variables * /
    Other_RAC = oRAC  # / * NO_INTENT, DO_NOT_CLIMB, DO_NOT_DESCEND * /
    Other_Capability = oc  # / * TCAS_TA, OTHER * /
    Climb_Inhibit = ci  # / * true / false * /

    Positive_RA_Alt_Thresh = [None, None, None, None]  # = new int[4];

    # initialize();
    Positive_RA_Alt_Thresh[0] = 400
    Positive_RA_Alt_Thresh[1] = 500
    Positive_RA_Alt_Thresh[2] = 640
    Positive_RA_Alt_Thresh[3] = 740

    enabled = High_Confidence and (Own_Tracked_Alt_Rate <= OLEV) and (
            Cur_Vertical_Sep > MAXALTDIFF)
    tcas_equipped = Other_Capability == TCAS_TA
    intent_not_known = Two_of_Three_Reports_Valid and Other_RAC == NO_INTENT

    alt_sep = UNRESOLVED

    if enabled and ((tcas_equipped and intent_not_known) or not tcas_equipped):
        upward_preferred = (Up_Separation + NOZCROSS if Climb_Inhibit else Up_Separation) > Down_Separation
        if upward_preferred:
            result = not (Own_Tracked_Alt < Other_Tracked_Alt) or (
                    (Own_Tracked_Alt < Other_Tracked_Alt) and (
                not (Down_Separation >= Positive_RA_Alt_Thresh[Alt_Layer_Value])))
        else:
            result = (Other_Tracked_Alt < Own_Tracked_Alt) and (
                    Cur_Vertical_Sep >= MINSEP) and (
                             Up_Separation >= Positive_RA_Alt_Thresh[Alt_Layer_Value])
        Non_Crossing_Biased_Climb = result

        upward_preferred = (
                               Up_Separation + NOZCROSS if Climb_Inhibit else Up_Separation) > Down_Separation
        if upward_preferred:
            result = (Own_Tracked_Alt < Other_Tracked_Alt) and (
                    Cur_Vertical_Sep >= MINSEP) and (
                             Down_Separation >= Positive_RA_Alt_Thresh[Alt_Layer_Value])
        else:
            result = not (Other_Tracked_Alt < Own_Tracked_Alt) or (
                    (Other_Tracked_Alt < Own_Tracked_Alt) and (
                    Up_Separation >= Positive_RA_Alt_Thresh[Alt_Layer_Value]))

        Non_Crossing_Biased_Descend = result

        need_upward_RA = Non_Crossing_Biased_Climb and (Own_Tracked_Alt < Other_Tracked_Alt)
        need_downward_RA = Non_Crossing_Biased_Descend and (Other_Tracked_Alt < Own_Tracked_Alt)
        # if need_upward_RA and need_downward_RA:
        #     alt_sep = UNRESOLVED             # unreachable
        # elif need_upward_RA:
        if need_upward_RA:
            alt_sep = UPWARD_RA
        elif need_downward_RA:
            alt_sep = DOWNWARD_RA
        else:
            alt_sep = UNRESOLVED

    return alt_sep


# print(tcas(2683134048, True, False, -2585122351, -3322476844, 2730189443, -2883773823, -145064337, -2782785870, 1807097934, -1866675354, False))
# print(tcas(601, True, True, 1, 1, 1, 1, 1, 1, 0, 1, True))
# print(tcas(1, True, True, 1, 1, 1, 1, 1, 1, 1, 1, True))
# print(tcas(627, False, False, 621,  216, 382, 1, 400,641, 1, 1, False))

def leapyear(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                print("1 ---- %d is a leap year." % year)
            else:
                print("2 ---- %d is not a leap year." % year)
        else:
            print("3 ---- %d is a leap year." % year)
    else:
        print("4 ---- %d is not a leap year." % year)

        # Returns    true if given    year is valid or not.


def isValidDate(y, m, d):
    MAX_VALID_YR = 10000
    MIN_VALID_YR = 0
    # If year, month and day  are not in given range
    if y > MAX_VALID_YR or y < MIN_VALID_YR:
        return False
    if m < 1 or m > 12:
        return False
    if d < 1 or d > 31:
        return False

    # Handle February month with leap year
    if m == 2:
        if ((y % 4 == 0) and (y % 100 != 0)) or (y % 400 == 0):
            return d <= 29
        else:
            return d <= 28

    # Months of April, June, Sept and Nov must have number of days less than or equal to 30.
    if m == 4 or m == 6 or m == 9 or m == 11:
        return d <= 30

    return True


# leapyear(1900)
# leapyear(2012)


def quadratic_equation(a, b, c):
    if a == 0:
        print("not quadratic")
    else:
        d = (b * b) - (4 * a * c)
        if d == 0:
            return "one root"
        else:
            if d > 0:
                return "two roots"
            else:
                return "imaginary roots"


# quadratic_equation(1, -3, 2)
# quadratic_equation(1, 2, 1)

def point_circle(x, y, r):
    if (x * x) + (y * y) > (r * r):
        return 'outside'
    elif (x * x) + (y * y) == (r * r):
        return 'on circle'
    else:
        return 'inside'


def bessj(n, x):
    """
    Bessel function J-sub-n(x).
    @see: NRP 6.5

    @param x: float number
    @return: float number

    @status: Tested function
    @since: version 0.1
    """
    import math

    print(n, x)
    iacc = 40.0
    bigno = 1.0e10
    bigni = 1.0e-10
    if n < 2:
        return 'n must be more than 1 - use bessj0 or bessj1 for n = 0 or 1 respectively'  # raise Exception
    if n > 1000:
        return
    if x == 0.0:
        ans = 0.0
    else:
        if abs(x) > 1.0 * n:
            tox = 2.0 / abs(x)
            # bjm = bessj0(abs(x))
            if abs(x) < 8.0:
                y = x * x
                bjm = (57568490574.0 + y * (-13362590354.0 + y * (651619640.7 +
                                                                  y * (-11214424.18 + y * (77392.33017 + y *
                                                                                           (-184.9052456)))))) / \
                      (57568490411.0 + y * (1029532985.0 + y * (9494680.718 + y *
                                                                (59272.64853 + y * (267.8532712 + y * 1.0)))))
            else:
                ax = abs(x)
                z = 8.0 / ax
                y = z * z
                xx = ax - 0.785398164
                ans1 = 1.0 + y * (-0.1098628627e-2 + y * (0.2734510407e-4 + y *
                                                          (-0.2073370639e-5 + y * 0.2093887211e-6)))
                ans2 = -0.156249995e-1 + y * (0.1430488765e-3 + y *
                                              (-0.6911147651e-5 +
                                               y * (0.7621095161e-6 - y * 0.934945152e-7)))
                bjm = math.sqrt(0.636619772 / ax) * (math.cos(xx) * ans1 - z *
                                                     math.sin(xx) * ans2)

            # bj = bessj1(abs(x))
            if abs(x) < 8.0:
                y = x * x
                ans1 = x * (72362614232.0 + y * (-7895059235.0 + y *
                                                 (242396853.1 + y *
                                                  (-2972611.439 + y * (15704.4826 + y * (-30.16036606))))))
                ans2 = 144725228442.0 + y * (2300535178.0 + y * (18583304.74 + y *
                                                                 (99447.43394 + y * (376.9991397 + y))))
                bj = ans1 / ans2
            else:
                ax = abs(x)
                z = 8.0 / ax
                y = z * z
                xx = ax - 2.356194491
                ans1 = 1.0 + y * (0.183105e-2 + y * (-0.3516396496e-4 + y *
                                                     (0.2457520174e-5 + y * (-0.240337019e-6))))
                ans2 = 0.04687499995 + y * (-0.2002690873e-3 + y *
                                            (0.8449199096e-5 + y *
                                             (-0.88228987e-6 + y * 0.105787412e-6)))
                if x < 0.0:
                    bj = math.sqrt(0.636619772 / ax) * \
                         (math.cos(xx) * ans1 - z *
                          math.sin(xx) * ans2)
                else:
                    bj = -1 * math.sqrt(0.636619772 / ax) * \
                         (math.cos(xx) * ans1 -
                          z * math.sin(xx) * ans2)

            for j in range(1, n):
                bjp = j * tox * bj - bjm
                bjm = bj
                bj = bjp
            ans = bj
        else:
            tox = 2.0 / abs(x)
            m = int(2 * ((n + math.floor(math.sqrt(1.0 * (iacc * n)))) % 2))
            ans = 0.0
            jsum = 0.0
            sum = 0.0
            bjp = 0.0
            bj = 1.0
            for j in range(m, 1, -1):
                bjm = j * tox * bj - bjp
                bjp = bj
                bj = bjm
                if abs(bj) > bigno:
                    bj = bj * bigni
                    bjp = bjp * bigni
                    ans = ans * bigni
                    sum = sum * bigni
                if jsum != 0: sum = sum + bj
                jsum = 1 - jsum
                if j == n: ans = bjp
            sum = 2.0 * sum - bj
            print(sum, ans)
            ans = ans / sum
        if x < 0.0 and (n % 2) == 1:
            ans = -ans
        return ans

        # def bessj0(x):
        #     """
        #     Bessel function J-sub-0(x).
        #     @see: NRP 6.4
        #
        #     @param x: float number
        #     @return: float number
        #
        #     @status: Tested function
        #     @since: version 0.1
        #     """
        #     if abs(x) < 8.0:
        #         y = x * x
        #         return (57568490574.0 + y * (-13362590354.0 + y * (651619640.7 + \
        #                                                            y * (-11214424.18 + y * (77392.33017 + y * \
        #                                                                                     (-184.9052456)))))) / \
        #                (57568490411.0 + y * (1029532985.0 + y * (9494680.718 + y * \
        #                                                          (59272.64853 + y * (267.8532712 + y * 1.0)))))
        #     else:
        #         ax = abs(x)
        #         z = 8.0 / ax
        #         y = z * z
        #         xx = ax - 0.785398164
        #         ans1 = 1.0 + y * (-0.1098628627e-2 + y * (0.2734510407e-4 + y * \
        #                                                   (-0.2073370639e-5 + y * 0.2093887211e-6)))
        #         ans2 = -0.156249995e-1 + y * (0.1430488765e-3 + y * \
        #                                       (-0.6911147651e-5 + \
        #                                        y * (0.7621095161e-6 - y * 0.934945152e-7)))
        #         return math.sqrt(0.636619772 / ax) * (math.cos(xx) * ans1 - z * \
        #                                               math.sin(xx) * ans2)
        #
        #
        # def bessj1(x):
        #     """
        #     Bessel function J-sub-1(x).
        #     @see: NRP 6.4
        #
        #     @param x: float number
        #     @return: float number
        #
        #     @status: Tested function
        #     @since: version 0.1
        #     """
        #     if abs(x) < 8.0:
        #         y = x * x
        #         ans1 = x * (72362614232.0 + y * (-7895059235.0 + y * \
        #                                          (242396853.1 + y * \
        #                                           (-2972611.439 + y * (15704.4826 + y * (-30.16036606))))))
        #         ans2 = 144725228442.0 + y * (2300535178.0 + y * (18583304.74 + y * \
        #                                                          (99447.43394 + y * (376.9991397 + y))))
        #         return ans1 / ans2
        #     else:
        #         ax = abs(x)
        #         z = 8.0 / ax
        #         y = z * z
        #         xx = ax - 2.356194491
        #         ans1 = 1.0 + y * (0.183105e-2 + y * (-0.3516396496e-4 + y * \
        #                                              (0.2457520174e-5 + y * (-0.240337019e-6))))
        #         ans2 = 0.04687499995 + y * (-0.2002690873e-3 + y * \
        #                                     (0.8449199096e-5 + y * \
        #                                      (-0.88228987e-6 + y * 0.105787412e-6)))
        #         if x < 0.0:
        #             return math.sqrt(0.636619772 / ax) * \
        #                    (math.cos(xx) * ans1 - z * \
        #                     math.sin(xx) * ans2)
        #         else:
        #             return -1 * math.sqrt(0.636619772 / ax) * \
        #                    (math.cos(xx) * ans1 - \
        #                     z * math.sin(xx) * ans2)


def sym_try(a):
    a = 13 + a
    if a + 1 > 30:
        return 3
        if a * a < 12:
            return 4
        else:
            return c


def sym_try2(a, b):
    a += b
    c = a * b ** 2
    if a + 1 > 30:
        return 3
    if a * a < 12:
        return 4
    else:
        return c


def sym_pow_test(a):
    if a < 0:
        return -1

    if a ** 2 == 4:
        return 0

    if a < 20:
        if 2 ** a >= 100:
            return 1
        else:
            return 3
    else:
        return 2


def sym_test_2(a, b):
    if a == 0 or b == 4:
        return 1
    if a == 2 and b == 2:
        return 2


def ttt(a, b, c):
    from math import log, sin

    if log(a) > 0:
        if sin(b) == 0:
            return 3
    if a + b > 0:
        if log(a + b) > c:
            return 1
    if a + b == 2 * c - 10:
        return 2
    if 2 * a > 100:
        return 4


def power(x):
    if (x + 2) ** 2 == 4:
        return 0
    elif x < 100:
        if 2 ** (x + 1) == 16:
            return 1
        else:
            return 2


def power2(x, y):
    from math import log
    if x > 100:
        return 0
    else:
        if log(x) == log(y):
            return 1
        else:
            if 2 ** (x + 1) == 16:
                return 2
            else:
                return 3


def p3(x, y):
    if x ** 3 + 1 == y:
        return 1
    else:
        return 2


def newton_ln(c, x0):
    from math import log
    x_n = x0
    x_n_1 = x_n * (1 - log(x_n) - c)
    epsilon = 0.01
    it = 0
    while it < 100 and abs(x_n_1 - x_n) > epsilon:
        x_n = x_n_1
        print(x_n)
        x_n_1 = x_n * (1 - log(x_n) - c)
        print(it, x_n_1)
        it += 1

    return x_n_1


def newton_exp(c, x0):
    from math import exp
    x_n = x0
    x_n_1 = x_n - (exp(x_n) - c) / exp(x_n)
    epsilon = 0.0001
    it = 0
    while it < 1000 and abs(x_n_1 - x_n) > epsilon:
        x_n = x_n_1
        x_n_1 = (exp(x_n) - c) / exp(x_n)
        # print(it, colored(x_n_1, 'blue'), exp(x_n_1))
        it += 1

    return x_n_1


# print(newton_exp(1, 10))

def trapezoid(a, b):
    import math
    f = lambda x: math.sqrt(x) * math.cos(x)

    if a >= b:
        return 0

    Iold = 0.0
    for k in range(1, 11):
        if k == 1:
            Inew = (f(a) + f(b)) * (b - a) / 2.0
        else:
            n = 2 ** (k - 2)
            h = (b - a) / n
            x = a + h / 2.0
            sum = 0.0
            for i in range(n):
                sum = sum + f(x)
                x = x + h
            Inew = (Iold + h * sum) / 2.0
        if (k > 1) and (abs(Inew - Iold)) < 1.0e-3:
            break
        Iold = Inew

    return Inew


# print(trapezoid(0, math.pi))

def bisection(x1, x2):
    import math

    tol = 1.0e-3
    f = lambda x: x ** 2 - 9

    f1 = f(x1)
    if f1 == 0.0:
        return x1
    f2 = f(x2)
    if f2 == 0.0:
        return x2
    if f1 > 0 and f2 > 0 or f1 < 0 and f2 < 0:
        # print('Root is not bracketed')
        return None

    n = int(math.ceil(math.log(abs(x2 - x1) / tol) / math.log(2.0)))
    for i in range(n):
        x3 = 0.5 * (x1 + x2)
        f3 = f(x3)
        if f3 == 0.0: return x3
        if f2 > 0 and f3 < 0 or f2 < 0 and f3 > 0:
            x1 = x3
            f1 = f3
        else:
            x2 = x3
            f2 = f3
    return (x1 + x2) / 2.0


# print(bisection(0.0, 1000.0, tol = 1.0e-20))

def expr(a, b, c):
    if a > 0 and b > 0 and c > 0 and b * b + c * c == a * a:
        return 1


def compute_tax(status, income):
    tax = 0
    if status == 0:
        if income <= 8350:
            tax = income * 0.10

        elif income <= 33950:
            tax = 8350 * 0.10 + (income - 8350) * 0.15
        elif income <= 82250:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (income - 33950) * 0.25
        elif income <= 171550:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (82250 - 33950) * 0.25 + (income - 82250) * 0.28
        elif income <= 372950:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (82250 - 33950) * 0.25 + \
                  (171550 - 82250) * 0.28 + (income - 171550) * 0.33
        else:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (82250 - 33950) * 0.25 + \
                  (171550 - 82250) * 0.28 + (372950 - 171550) * 0.33 + (income - 372950) * 0.35

    elif status == 1:
        if income <= 16700:
            tax = income * 0.10
        elif income <= 67900:
            tax = 16700 * 0.10 + (income - 16700) * 0.15
        elif income <= 137050:
            tax = 16700 * 0.10 + (67900 - 16700) * 0.15 + (income - 67900) * 0.25
        elif income <= 208850:
            tax = 16700 * 0.10 + (67900 - 16700) * 0.15 + (137050 - 67900) * 0.25 + (income - 137050) * 0.28
        elif income <= 372950:
            tax = 16700 * 0.10 + (67900 - 16700) * 0.15 + (137050 - 67900) * 0.25 + \
                  (208850 - 137050) * 0.28 + (income - 208850) * 0.33
        else:
            tax = 16700 * 0.10 + (67900 - 16700) * 0.15 + (137050 - 67900) * 0.25 + \
                  (208850 - 137050) * 0.28 + (372950 - 208850) * 0.33 + (income - 372950) * 0.35
    elif status == 2:
        if income <= 8350:
            tax = income * 0.10
        elif income <= 33950:
            tax = 8350 * 0.10 + (income - 8350) * 0.15
        elif income <= 68525:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (income - 33950) * 0.25
        elif income <= 104425:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (68525 - 33950) * 0.25 + (income - 68525) * 0.28
        elif income <= 186475:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (68525 - 33950) * 0.25 + \
                  (104425 - 82250) * 0.28 + (income - 104425) * 0.33
        else:
            tax = 8350 * 0.10 + (33950 - 8350) * 0.15 + (68525 - 33950) * 0.25 + \
                  (104425 - 68525) * 0.28 + (186475 - 104425) * 0.33 + (income - 186475) * 0.35
    elif status == 3:
        if income <= 11950:
            tax = income * 0.10
        elif income <= 45500:
            tax = 11950 * 0.10 + (income - 11950) * 0.15
        elif income <= 117450:
            tax = 11950 * 0.10 + (45500 - 11950) * 0.15 + (income - 45500) * 0.25
        elif income <= 190200:
            tax = 11950 * 0.10 + (45500 - 11950) * 0.15 + (117450 - 45500) * 0.25 + (income - 117450) * 0.28
        elif income <= 372950:
            tax = 11950 * 0.10 + (45500 - 11950) * 0.15 + (117450 - 45500) * 0.25 + \
                  (190200 - 117450) * 0.28 + (income - 190200) * 0.33
        else:
            tax = 11950 * 0.10 + (45500 - 11950) * 0.15 + (117450 - 45500) * 0.25 + \
                  (190200 - 117450) * 0.28 + (372950 - 190200) * 0.33 + (income - 372950) * 0.35

    return tax


# Function to print the calendar of the given year
def print_calendar(year):
    if year < 0 or year > 2100:
        return

    # print("		 Calendar - %d\n\n" % year)

    ##### Index of the day from 0 to 6
    t = [0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4]
    y = year
    month = 1
    day = 1
    y -= 1 if month < 3 else 0
    current = (y + y // 4 - y // 100 +
               y // 400 + t[month - 1] + day) % 7

    # i --> Iterate through all the months
    #  j --> Iterate through all the days of the
    #	 month - i
    for i in range(12):
        # January
        if i == 0:
            days = 31

        # February
        if i == 1:
            # If the  year is leap   then  February  has 29 days
            if (year % 400 == 0 or
                    (year % 4 == 0 and year % 100 != 0)):
                days = 29
        else:
            days = 28

        # March
        if i == 2:
            days = 31

        # April
        if i == 3:
            days = 30

        # May
        if i == 4:
            days = 31

        # June
        if i == 5:
            days = 30

        # July
        if i == 6:
            days = 31

        # August
        if i == 7:
            days = 31

        # September
        if i == 8:
            days = 30

        # October
        if i == 9:
            days = 31

        # November
        if i == 10:
            days = 30

        # December
        if i == 11:
            days = 31

        # Print the current month name
        # print("\n ------------%s-------------\n" %
        #       ["January", "February", "March",
        #        "April", "May", "June",
        #        "July", "August", "September",
        #        "October", "November", "December"][i])

        # Print the columns
        # print(" Sun Mon Tue Wed Thu Fri Sat\n")

        # Print appropriate spaces

        k = 0
        for k in range(current):
            pass
            # print("	 ", end='')

        for j in range(1, days + 1):
            # print("%5d" % j, end='')

            k += 1
            if k > 6:
                k = 0
                # print()

            if k == 0:
                # print("\n")
                pass

            current = k


# print_calendar(2019)


def line_rectangle(xr1, yr1, xr2, yr2, xl1, xl2, yl1, yl2):
    salida = -1
    # precondiciones
    if xr1 < xr2 and yr1 < yr2 and xl1 <= xl2:  # //b1

        # completamente cubierta
        if (xl1 >= xr1 and xl1 <= xr2 and xl2 >= xr1 and xl2 <= xr2 and yl1 >= yr1 and yl1 <= yr2
                and yl2 >= yr1 and yl2 <= yr2):  # //b2
            salida = 1

        else:
            if yl1 == yl2:  # //b4 // linea horizantal
                if yl1 < yr1:  # //b5
                    salida = 2
                elif yl1 > yr2:  # //b6
                    salida = 2
                else:  # //b7
                    if xl1 > xr2 or xl2 < xr1:  # //b8
                        salida = 2
                    else:  # //b9
                        salida = 3  # // parcialmente cubierta
                # //b10

            elif xl1 == xl2:  # //b11 // linea vertical
                if xl1 < xr1:  # //b12
                    salida = 2
                elif xl1 > xr2:  # //b13
                    salida = 2
                else:  # b14
                    if yl1 > yr2 or yl2 < yr1:  # //b15
                        salida = 2
                    else:  # //b16
                        salida = 3
                # //b17

            else:  # // linea inclinada
                if yl1 < yr1 and yl2 < yr1:  # //b19 // parte superior del rectangulo
                    salida = 2
                elif yl1 > yr2 and yl2 > yr2:  # //b20 // parte inferior del rectangulo
                    salida = 2
                elif xl1 < xr1 and xl2 < xr1:  # //b21 // parte izquierda del rectangulo
                    salida = 2
                elif xl1 > xr2 and xl2 > xr2:  # //b22 // parte derecha del rectangulo
                    salida = 2
                else:
                    # // pto de corte con la parte superior del rectangulo
                    x = (xl2 - xl1) // (yl2 - yl1) * (yr1 - yl1) + xl1
                    if x >= xr1 and x <= xr2:  # //b24
                        salida = 3
                    else:
                        x = (xl2 - xl1) // (yl2 - yl1) * (yr2 - yl1) + xl1
                        if x >= xr1 and x <= xr2:  # //b26
                            salida = 3
                        else:
                            y = (yl2 - yl1) // (xl2 - xl1) * (xr1 - xl1) + yl1
                            if y >= yr1 and y <= yr2:  # //28
                                salida = 3
                            else:
                                # // pto de corte con la parte derecha del rectangulo
                                y = (yl2 - yl1) // (xl2 - xl1) * (xr2 - xl1) + yl1
                                if y >= yr1 and y <= yr2:  # //b30
                                    salida = 3
                                else:  # //b31
                                    salida = 2  # // la linea no corta al rectangulo

    return salida


# line_rectangle(0, 0, 10, 10, -1, 7, 10, 0)

def fisher(x, d1, d2):
    #		int a, b, i, j;
    #		double w, y, z, zk, d, p;
    import math

    if d1 > 1000 or d2 > 1000: return

    a = 2 * (d1 // 2) - d1 + 2
    b = 2 * (d2 // 2) - d2 + 2
    w = (x * d1) / d2
    z = 1.0 / (1.0 + w)

    if a == 1:
        if b == 1:
            p = math.sqrt(w)
            y = 0.3183098862
            d = y * z / p
            p = 2.0 * y * math.atan(p)

        else:
            p = math.sqrt(w * z)
            d = 0.5 * p * z / w


    elif b == 1:

        p = math.sqrt(z)
        d = 0.5 * z * p
        p = 1.0 - p

    else:

        d = z * z
        p = w * z

    y = 2.0 * w / z

    if a == 1:
        for j in range(b + 2, d2 + 1, 2):  # (j = b+2; j <= n; j += 2)
            d *= (1.0 + 1.0 / (j - 2)) * z
            p += d * y / (j - 1)

    else:
        zk = z ** ((d2 - 1) / 2)
        d *= (zk * d2) / b
        p = p * zk + w * z * (zk - 1.0) / (z - 1.0)

    y = w * z
    z = 2.0 / z
    b = d2 - 2
    for i in range(a + 2, d1 + 1, 2):  # (i = a+2; i <= m; i += 2):

        j = i + b
        d *= (y * j) / (i - 2)
        p -= z * d / j

    if p < 0.0:
        return 0.0
    elif p > 1.0:
        return 1.0
    else:
        return p

# print(fisher(10, 7, 3))      # 0.9574706798668541
# print(fisher(1,2,1))            # 0.42264973081037427