import math


class SimilarityCalculator:

    def sim_smc(self, a, b):
        c00 = c01 = c10 = c11 = 0

        for i in range(len(a)):
            if a[i] == 0 and b[i] == 0:
                c00 += 1
            elif a[i] == 0 and b[i] == 1:
                c01 += 1
            elif a[i] == 1 and b[i] == 0:
                c10 += 1
            else:
                c11 += 1

        s = (c00 + c11) / (c00 + c01 + c10 + c11)
        return s

    def sim_jaccard(self, a, b):
        c00 = c01 = c10 = c11 = 0

        for i in range(len(a)):
            if a[i] == 0 and b[i] == 0:
                c00 += 1
            elif a[i] == 0 and b[i] == 1:
                c01 += 1
            elif a[i] == 1 and b[i] == 0:
                c10 += 1
            else:
                c11 += 1

        s = c11 / (c01 + c10 + c11)
        return s

    def sim_cos(self, a, b):
        aDotb = 0
        aNorm = 0
        bNorm = 0

        for i in range(len(a)):
            aDotb += a[i] * b[i]
            aNorm += a[i] * a[i]
            bNorm += b[i] * b[i]
        aNorm = math.sqrt(aNorm)
        bNorm = math.sqrt(bNorm)

        if aNorm == 0 or bNorm == 0:
            return 0
        return aDotb / (aNorm * bNorm)

    def norm(self, a):
        aNorm = 0
        for i in range(len(a)):
            aNorm += a[i] * a[i]
        return math.sqrt(aNorm)

    def sim_path(self, target, cov, obj_vec):
        if tuple(target) == tuple(cov):
            return 1

        for i in range(len(obj_vec)):
            obj_vec[i] *= target[i]
            if target[i] == 1:
                if cov[i] == 1:
                    obj_vec[i] = 1
                else:
                    obj_vec[i] -= 0.1
                    if obj_vec[i] < 0:
                        obj_vec[i] = 0
        s = self.sim_cos(target, obj_vec)
        if s == 1.0: # angle = 0 (but not necessarily exactly equal)
            if self.norm(target) != self.norm(obj_vec):
                return self.norm(obj_vec)   # in case of only one branch like target=(1,0,0), obj_vec=(0.8, 0, 0) ---> sim_cos = 1 but norm = 0.8
            return 1
        return s