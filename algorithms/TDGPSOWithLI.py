import random

from algorithms.TDGPSO import TDGPSO, Particle
from common.commons import Color, get_invariants_for_cov


class TDGPSOWithLI(TDGPSO):
    def __init__(self, program, goals):
        TDGPSO.__init__(self, program, goals)

        self.USE_PROGRAM_CONSTANTS = True
        self.USE_LI_FOR_INIT = True
        self.CONSTANT_FOR_INIT_PROB = 0.1
        self.INIT_WITH_LI_PROB = 0.5


        self.gbest = dict()

    def initial_population(self):
        self.population = set()  # not a set in pso

        for i in range(self.POPULATION_SIZE):
            particle = Particle(self.generate_random_data())  # a single random data based on types
            self.population.add(particle)

        self.after_initial_population()

    def after_initial_population(self):
        if self.USE_LI_FOR_INIT:
            self.population = set(
                random.sample(self.population, int(self.POPULATION_SIZE * (1 - self.INIT_WITH_LI_PROB))))

            self.new_population(self.generate_initial_populaton_with_li())

        # partition_size = self.POPULATION_SIZE // len(self.goals)
        # for g in self.goals:
        it = iter(self.goals)
        for i, p in enumerate(self.population):
            # if i % partition_size == 0:
            cur_g = next(it, None)
            if cur_g is None:
                it = iter(self.goals)
                cur_g = next(it, None)

            p.g = cur_g  # assign this goal to particle p

        for g in self.goals:
            self.gbest[g] = self.get_best_by_goal(g).prog_input

        print("INITIAL POP WITH LI: ", self.population)
        print("gbest: ", self.gbest)

    def iteration_operation_on_population(self):
        for prtcl in self.population:
            particle_goal = prtcl.g
            if self.fitness_by_goal(prtcl, particle_goal) == 1:
                continue

            newV = list(prtcl.V)
            newX = list(prtcl.prog_input)  # tuple to list

            # loop through all the particles in population:
            program_input_types = self.program.get_input_types()
            for d in range(len(program_input_types)):

                # update velocity for dimension d:
                newV[d] = int(min(self.w * prtcl.V[d] +
                                  self.c1 * self.r1 * (prtcl.pbest[d] - prtcl.prog_input[d]) +
                                  self.c2 * self.r2 * (self.gbest[particle_goal][d] - prtcl.prog_input[d]),
                                  Particle.vmax))

                # update position for dimenstion d:
                if program_input_types[d] is int:
                    newX[d] += newV[d]
                    newX[d] = self.clip(newX[d])
                elif program_input_types[d] is bool:
                    if newV[d] > Particle.vmax / 2 or newV[d] < -Particle.vmax / 2:  # don't change for slow velocities, but change probabilistically for fast changes
                        newX[d] = random.choice([True, False])

            prtcl.prog_input = tuple(newX)
            prtcl.V = tuple(newV)

            self.program.add_input(prtcl.prog_input)

            f = self.fitness(prtcl)
            f_pbest = self.fitness_by_goal(Particle(prtcl.pbest), particle_goal)
            f_gbest = self.fitness_by_goal(Particle(self.gbest[particle_goal]), particle_goal)

            if f > f_pbest:
                prtcl.pbest = prtcl.prog_input

            if f > f_gbest:
                self.gbest[particle_goal] = prtcl.prog_input

        return self.population

    def after_iteration_multi(self, bests):
        # if (True):
        #     return
        for b in bests:
            print(Color.red, b, Color.reset)
            coverage = self.program.get_coverage(b.prog_input)
            inv_best = get_invariants_for_cov(self.program.lig, coverage)[1]

            if self.verbose:
                print(Color.darkgrey, "invariants for best: ", inv_best, Color.reset)

            inputs_like_best = self.generate_values_for_inv(inv_best, coverage, self.NEW_INDIVIDUAL_COUNT)
            self.new_population(inputs_like_best)

            self.after_new_inputs_by_cs(inputs_like_best, b)  # added for PSO-li (setting V and pbest)

            # PLI mutation:
            inputs_like_mut_best = self.mutate_invs_and_generate_values(inv_best)
            self.new_population(inputs_like_mut_best)

            self.after_new_inputs_by_cs(inputs_like_mut_best, b)  # added for PSO-li (setting V and pbest)

    @staticmethod
    def after_new_inputs_by_cs(new_inputs, best):
        for prtcl in new_inputs:
            prtcl.g = best.g
            prtcl.pbest = best.pbest
            #         self.V[inp] = tuple(random.sample(range(-self.vmax, self.vmax), len(best)))#self.V[best]
            #         self.pbest[inp] = tuple([0 for i in range(len(best))])#inp
