import cProfile
import random
import time

from algorithms.TestDataGenerationAlgorithm import TestDataGenerationAlgorithm, Individual
from common.ConstraintSolver import ConstraintSolver
from common.SimilarityCalculator import SimilarityCalculator
from common.commons import Color
from common.likelyinvariant import LikelyInvariant


class TDGSearchBased(TestDataGenerationAlgorithm):
    def __init__(self, program, goals):
        super().__init__(program, goals)

        self.NEW_INDIVIDUAL_COUNT = 5

        self._fitness_by_goal = dict()

        self.INDIVIDUAL_TYPE = Individual      # can be Chromosome, Particle, or Ant too

        self.INIT_WITH_LI_PROB = 0.5
        self.CONSTANT_FOR_INIT_PROB = 0.1

    def fitness_by_goal(self, indv, goal):
        if self._fitness_by_goal.get((indv, goal)) is not None:
            return self._fitness_by_goal[(indv, goal)]

        self.calc_fitness_by_goal(indv, goal)
        return self._fitness_by_goal[(indv, goal)]

    def calc_fitness_by_goal(self, indv, goal):
        coverage = self.program.get_coverage(indv.prog_input)
        obj_vec = self.program.get_objectivefunc(indv.prog_input)
        self._fitness_by_goal[(indv, goal)] = SimilarityCalculator().sim_path(goal, coverage, obj_vec)

    def new_population(self, offsprings):
        self.population.update(offsprings)
        # sorted_pop = set(self.population)
        # sorted_pop.update(offsprings)

        # sorted_pop_list = list(sorted_pop)
        # sorted_pop_list.sort( #TODO
        #     key=lambda p: self.fitness(self.program.get_coverage(p), self.program.get_objectivefunc(p)), reverse=True)
        #
        # return sorted_pop_list[:self.population_count]  # slice first chromosomes

    def generate_data(self):
        self.iteration_count = 0

        self.initial_population()

        iteration_counts = []
        found_counts = []
        fitnesses = []

        found_count = 0

        while self.check_population_against_goals() < 100 and  \
                ((not self.TERMINATION_BY_TIME and self.iteration_count < self.MAX_ITERATION) \
                or (self.TERMINATION_BY_TIME and self.run_time <= self.MAX_TIME)): # and found_count < self.TARGET_COUNT:
            self.iteration_count += 1
            iteration_counts.append(self.iteration_count)

            if not self.MULTIPLE_GOALS:
                found_count = self.single_goal_iteration(fitnesses, found_counts)
            else:
                found_count = self.multi_goal_iteration(fitnesses, found_count, found_counts)

            self.run_time = int(time.time()) - self.START_TIME

        return iteration_counts, found_counts, fitnesses

    def initial_population(self):
        for i in range(self.POPULATION_SIZE):
            tup = self.generate_random_data()
            self.program.add_input(tup)

            self.population.add(self.INDIVIDUAL_TYPE(tup))
        print("initial population: ", self.population)

        self.after_initial_population()

    def single_goal_iteration(self, fitnesses, found_counts):
        offsprings = self.iteration_operation_on_population()

        self.new_population(offsprings)
        self.population = set(sorted(self.population,
                                     key = lambda p: self.fitness(p), reverse = True)[:self.POPULATION_SIZE])

        best = self.get_best_individual()
        if self.verbose:
            print("fit: ", self.program.get_objectivefunc(best))
            fitness = self.fitness(best)
            print(Color.lightblue if fitness != 1 else Color.green, "best: ", best, "fitness: ", fitness,
                  self.program.get_coverage(best), Color.reset)

        # pr = cProfile.Profile()
        # pr.enable()
        self.after_iteration(best)
        # pr.disable()
        # pr.print_stats(sort="tottime")
        found_count = sum(1 for x in self.population if self.fitness(x) == 1)

        found_counts.append(found_count)
        fitnesses.append(self.fitness(best))
        return found_count

    def iteration_operation_on_population(self):
        return self.population # do nothing (inherited classes change this- ga: mutate and crossover)

    def multi_goal_iteration(self, fitnesses, found_count, found_counts):

        offsprings = self.iteration_operation_on_population()
        self.new_population(offsprings)

        self.calc_fitness_by_goals()

        islands = self.cluster_by_branches()

        for k, v in islands.items():
            print(Color.green, k, Color.reset)
            print(Color.green, v, Color.reset)
        print('---')


        bests = set()
        for br, tops in islands.items():
            best = tops[0]
            bests.add(best)

        # pr = cProfile.Profile()
        # pr.enable()
        self.after_iteration_multi(bests)
        # pr.disable()
        # pr.print_stats(sort="tottime")

        # found_count = sum(1 for x in self.population if
        #                   self.fitness(self.program.get_coverage(x), self.program.get_objectivefunc(x)) == 1)

        islands = self.cluster_by_branches()

        # keep only the top solutions for each goal in population, and discard the rest
        new_pop = set()
        for br, tops in islands.items():
            new_pop.update(tops[:self.GOAL_PARTITION_SIZE])
        # self.population = new_pop

        if len(new_pop) < self.POPULATION_SIZE:        # there may be some overlappings in islands. we add the remaining members for not losing the population size
            size = min(len(self.population), self.POPULATION_SIZE) - len(new_pop)
            if size > 0:
                new_pop.update([indv for indv in self.population if indv not in new_pop][:size])

        self.population = new_pop

        print(self.population)

        found_counts.append(found_count)
        fitnesses.append(self.fitness(best))
        return found_count


    def calc_fitness_by_goals(self):
        for p in self.population:
            for g in self.goals:
                self.calc_fitness_by_goal(p, g)

    def cluster_by_branches(self):
        islands = dict()

        for g in self.goals:
            islands[g] = sorted(self.population, key=lambda p: self.fitness_by_goal(p, g),
                                reverse=True)[:self.GOAL_PARTITION_SIZE] # top solutions for this goal

            # for randomizing the top solution:
            best = islands[g][0]
            tops = set([p for p in islands[g] if self.fitness_by_goal(p, g) == self.fitness_by_goal(best, g)])
            islands[g][0] = random.sample(tops, 1)[0]         # set member set of the sorted list one of the tops (randomly)

        return islands

    # @abstractmethod
    def after_iteration(self, best):
        pass

    def after_iteration_multi(self, best):
        pass

    def after_initial_population(self):
        pass

    def generate_initial_populaton_with_li(self):
        inputs = set()
        params = self.program.get_input_types()

        ili = self.program.lig.init_likely_invariants()

        lis = set()

        for i in range(int(self.POPULATION_SIZE * self.INIT_WITH_LI_PROB)):
            size = random.randint(1, len(params))
            lis.add(tuple(random.sample(ili, size)))

        cs = ConstraintSolver()
        for li in lis:
            if self.verbose:
                print(li)
            cons = ConstraintSolver.make_constraint(li)
            values_for = cs.solve_constraint(cons, params, set(), False)
            if values_for is not None:
                values_for = tuple(values_for)
                if self.verbose:
                    print("initial: ", values_for)
                inputs.add(self.INDIVIDUAL_TYPE(values_for))
                self.program.add_input(values_for)

        return inputs

    def generate_values_for_inv(self, inv, coverage, n):

        inputs = set()
        params = self.program.get_input_types()
        cons = ConstraintSolver.make_constraint(inv)

        already_found = set(self.program.get_inputs_for_coverage(tuple(coverage)))
        if len(already_found) > 10:
            already_found = set(random.sample(already_found, 10))#set(list(already_found)[:10]) # TODO: empty: probabably it has to do with branch instead of path or something
        cs = ConstraintSolver()
        for i in range(n):

            values_for = cs.solve_constraint(cons, params, already_found, False)
            if values_for is not None:
                values_for = tuple(values_for)

                if self.verbose:
                    print(Color.darkgrey + "Generated: " + str(values_for) + Color.reset)

                inputs.add(self.INDIVIDUAL_TYPE(values_for))

                already_found.add(values_for)
                self.program.add_input(values_for)

        return inputs

    @staticmethod
    def mutate_inv(clause): # TODO maybe it's better to mutate using NOT in z3
        mut_ops = {'==': '!=',
                   '!=': '==',
                   '<': '>=',
                   '>': '<=',
                   '<=': '>',
                   '>=': '<'}

        return LikelyInvariant(clause.operands, mut_ops[clause.operator])

    def mutate_invs_and_generate_values(self, inv):

        inputs = set()
        cs = ConstraintSolver()
        for cl in inv:
            mut_inv = set(inv)
            mut_inv -= {cl}
            mut_inv.add(self.mutate_inv(cl))

            cons = ConstraintSolver.make_constraint(mut_inv)

            values_for = cs.solve_constraint(cons, self.program.get_input_types(), set(), False)
            if values_for is not None:
                values_for = tuple(values_for)

                inputs.add(self.INDIVIDUAL_TYPE(values_for))
                if self.verbose:
                    print(Color.darkgrey + "MUT-Generated: " + str(values_for) + Color.reset)

                self.program.add_input(values_for)

        return inputs

    def get_best_by_goal(self, g):
        return sorted(self.population, key=lambda p: self.fitness_by_goal(p, g),
               reverse=True)[0]

    def keep_best_individuals(self):
        self.population = set(sorted(self.population,
                                     key=lambda p: self.fitness(p), reverse=True)[:self.POPULATION_SIZE])