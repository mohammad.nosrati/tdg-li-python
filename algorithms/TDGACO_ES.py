import math
import random

from algorithms.TDGACO import Ant, TDGACO
from algorithms.TDGSearchBased import TDGSearchBased


class TDGACO_ES(TDGACO):
    def __init__(self, program, goals):
        super().__init__(program, goals)

        # self.tau_ES = dict()

        # self.already_covered = dict()
        # for g in self.goals:
        #     # self.tau_ES[g] = 1
        #     self.already_covered = False
        #
        # self.gbest = None  # best global partition
        #
        # self.USE_PROGRAM_CONSTANTS = False
        # self.INDIVIDUAL_TYPE = Ant
        #
        # self.ants = dict()
        #
        # self.rmax, self.q0, self.T, self.phi, self.rho0, self.alpha = 10, 0.5, 1, 0.5, 0.3, 0.3
        self.LS_itr = 10

    # def init_ants(self):

    #
    # def initial_population(self):
    #     self.population = set()  # not a set in pso
    #
    #     for i in range(self.POPULATION_SIZE):
    #         position = self.generate_random_data()
    #         ant = Ant(position)
    #
    #         self.population.add(ant)
    #
    #     self.after_initial_population()
    #
    #     self.gbest = self.get_best_individual()

        # for p in self.population:
        #     for g in self.goals:
        #         if self.covers_goal(p, g):
        #             self.already_covered = True
        # pass



    # def after_initial_population(self):
    #     if self.USE_LI_FOR_INIT:
    #         self.population = set(random.sample(self.population, self.POPULATION_SIZE // 4))
    #
    #         self.new_population(self.generate_initial_populaton_with_li())
    #
    #     print("INITIAL POP: ", self.population)

    # def iteration_operation_on_population(self):
    #
    #     for ant in self.population:
    #         self.local_search(ant)
    #
    #     self.global_search()
    #     self.gbest = self.get_best_individual()
    #     self.update_pheromone()
    #
    #     return self.population

    def local_search(self, ant):  # locally move ants in their neighbors
        count = 0

        while count < self.LS_itr:
            newpos = self.random_neighbor(ant)
            newant = Ant(newpos)
            if self.fitness(newant) > self.fitness(ant):
                ant.prog_input = newant.prog_input # or ant = newant
                break
            count += 1

        ant.prog_input = tuple(newpos)

    # def global_search(self):
    #     f_avg = sum(self.fitness(ant) for ant in self.population) / len(self.population)
    #
    #     for ant in self.population:
    #         if self.fitness(ant) < f_avg and random.random() < self.q0:
    #             ant.prog_input = self.generate_random_data()  # select a random position for low quality ant
    #
    #         else:
    #
    #             s = sum(a.tau * math.exp(self.fitness(a) - self.fitness(ant)) for a in self.population)
    #
    #             for other_ant in self.population:
    #                 if other_ant != ant and self.fitness(ant) < self.fitness(other_ant):
    #                     prob = other_ant.tau * math.exp(self.fitness(other_ant) - self.fitness(ant)) / s
    #                     if random.random() < prob:    # with the probability computed above, this is the target ant
    #                         # move to this ant
    #                         other_ant.record.add(ant)
    #                         if random.random() < self.rho0:
    #                             ant.prog_input = self.random_neighbor(other_ant)    # select a random neighboror of other ant
    #                         else:
    #                             ant.prog_input = self.move_towards(ant, other_ant)  # or just move towards it


    # def update_pheromone(self):
    #     for ant in self.population:
    #         ant.tau = (1 - self.alpha) * ant.tau + \
    #                   sum(self.fitness(a) for a in ant.record)  # sum of pheromones of moved ants to "ant"
    #     ant.record = set()

    # def random_neighbor(self, ant):
    #     newinp = []
    #     for d, type in enumerate(self.program.get_input_types()):
    #         if type == int:
    #             newinp.append(ant.prog_input[d] + random.randint(-self.rmax // 2, self.rmax // 2))
    #             # check for bounds:
    #             if newinp[d] > self.MAXINT:
    #                 newinp[d] = self.MAXINT
    #             if newinp[d] < self.MININT:
    #                 newinp[d] = self.MININT
    #         elif type == bool:
    #             newinp.append(random.randint(0, 1) == 1)
    #     return tuple(newinp)

    # def move_towards(self, source, dest):
    #     target_point = []
    #     types = self.program.get_input_types()
    #     for d in range(len(types)):
    #         if types[d] is int:
    #             target_point.append(
    #                 int(self.phi * dest.prog_input[d] + (1 - self.phi) * source.prog_input[d]))
    #         elif types[d] is bool:
    #             if source.prog_input[d] != dest.prog_input[d]:
    #                 # if they are different with higher probability change to dest's value
    #                 target_point.append(dest.prog_input[d] if random.random() < 0.7 else source.prog_input[d])
    #             else:
    #                 target_point.append(dest.prog_input[d])
    #
    #     return tuple(target_point)