import random

from algorithms.TDGSearchBased import TDGSearchBased
from algorithms.TestDataGenerationAlgorithm import Individual


class Chromosome(Individual):
    def to_bin(self, types, b):
        tup = self.prog_input
        tot_bits = ""

        for i, t in enumerate(tup):
            if types[i] is int:
                bits = ("{0:b}".format(t))
                if t < 0:
                    tot_bits += "-" + bits[1:].zfill(b)       # change -101 to -00...00101
                else:
                    tot_bits += "+" + bits.zfill(b)
            elif types[i] is bool:
                tot_bits += str(int(t))
                # tot_bits += ","
        return tot_bits

    @staticmethod
    def from_bin(chrom_bits, types, bitcount):
        out = []
        cursor = 0
        for t in types:
            if t is int:
                out.append(int(chrom_bits[cursor: cursor + bitcount + 1], 2))
                cursor += (bitcount + 1)
            elif t is bool:
                out.append(bool(int(chrom_bits[cursor: cursor + 1])))
                cursor += 1

        return Chromosome(tuple(out))

    @staticmethod
    def crossover_bits(t1, t2, types, bitcount):
        bits_p1 = t1.to_bin(types, bitcount)
        bits_p2 = t2.to_bin(types, bitcount)

        # print(bits_p1)
        # print(bits_p2)
        cross_point = random.randint(0, len(bits_p1) - 1)

        ch = bits_p1[:cross_point] + bits_p2[cross_point:]
        # print(' ' * cross_point + "^")
        # print(bits_p1[:cross_point])
        # print(' ' * cross_point + bits_p2[cross_point:])
        #
        # print(ch)
        return Chromosome.from_bin(ch, types, bitcount)


    def mutate(self, intbits):
        tup = self.prog_input
        m = [None] * len(tup)

        for i in range(len(tup)):
            if random.random() > 0.01:
                m[i] = tup[i]

            elif type(tup[i]) == int:  # ok, we are mutating. if this is int do this:
                rand_bit = random.randint(0, intbits - 1)  # 0 to 31
                m[i] = tup[i] ^ (2 ** rand_bit)
            elif type(tup[i]) == bool:
                m[i] = not tup[i]

        return Chromosome(tuple(m))



    def __repr__(self):
        return "C:" + str(self.prog_input)

## test:
# c = Chromosome((-3, True, 3))
# print(c)
# print(c.to_bin((int, bool, int), 5))
# print(c.from_bin('-000111+00111', (int, bool, int), 5))


class TDGGenetics(TDGSearchBased):
    def __init__(self, program, goals):
        super().__init__(program, goals)

        self.INDIVIDUAL_TYPE = Chromosome

    def iteration_operation_on_population(self):
        offsprings = self.crossover()
        offsprings = self.mutate_batch(offsprings)
        return offsprings

    def crossover(self):

        # parents = list(self.population)
        children = set()

        # use_crossover_bits = True

        # for i in range(len(self.population)):
        #     idx = random.randint(0, len(parents) - 1)
        #     p1 = parents[idx]
        #     idx = random.randint(0, len(parents) - 1)
        #     p2 = parents[idx]

            # if not use_crossover_bits:
            #     ch = [None] * len(p1)  # empty list of size len(p1)
            #     cross_point = random.randint(0, len(p1) - 1)
            #     for j in range(len(p1)):
            #         if j < cross_point:
            #             ch[j] = p1[j]
            #         else:
            #             ch[j] = p2[j]
            #
            #     ch_t = tuple(ch)
            #     self.program.add_input(ch_t)
            # else:
        for i in range(len(self.population)):
            parents = random.sample(self.population, 2)
            children.add(Chromosome.crossover_bits(parents[0], parents[1], self.program.get_input_types(), self.BITS_COUNT))

        return children

    def mutate_batch(self, offsprings):

        pop_set = set(offsprings)

        for p in self.population:
            m = p.mutate(self.BITS_COUNT)
            if p in pop_set:
                pop_set.remove(p)
            pop_set.add(m)

            self.program.add_input(m.prog_input)

        return pop_set