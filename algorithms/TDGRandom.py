import time

from algorithms.TestDataGenerationAlgorithm import TestDataGenerationAlgorithm, Individual
from common.commons import Color


class TDGRandom(TestDataGenerationAlgorithm):
    def __init__(self, program, goals):
        super().__init__(program, goals)
        # self.POPLUATION_SIZE = 1

    def generate_data(self):
        self.iteration_count = 0

        self.run_time = 0
        while self.check_population_against_goals() < 100 and  \
                ((not self.TERMINATION_BY_TIME and self.iteration_count < self.MAX_ITERATION) \
                or (self.TERMINATION_BY_TIME and self.run_time <= self.MAX_TIME)): # and found_count < self.TARGET_COUNT:
            self.iteration_count += 1
            self.population.clear()

            for i in  range(self.POPULATION_SIZE):
                self.population.add(Individual(self.generate_random_data()))

            print(self.population)
            self.run_time = int(time.time()) - self.START_TIME

        last, fit = sorted([(i, self.fitness(i)) for i in self.population]
                           , key=lambda t: t[1], reverse=True)[0]       # sort by fitness (t[1])

        print(Color.lightred, "last fitness: ", last, [fit], Color.reset)

        return self.iteration_count, [1 if fit == 1 else 0], [fit]