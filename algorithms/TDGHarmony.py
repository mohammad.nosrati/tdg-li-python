import random

from algorithms.TDGSearchBased import TDGSearchBased
from algorithms.TestDataGenerationAlgorithm import Individual




class Harmony(Individual):
    def __init__(self, prog_input):
        self.prog_input = prog_input
        self.b_range = 100 # max change

    # def __hash__(self):
    #     return hash((self.prog_input, self.V))
    #
    # def __eq__(self, other):
    #     return self.prog_input == other.prog_input and self.V == other.V

    def __repr__(self):
        return "H:" + str(self.prog_input)


class TDGHarmony(TDGSearchBased):
    def __init__(self, program, goals):
        super().__init__(program, goals)

        self.r_pa, self.r_hmc = 0.5, 0.75

        # self.gbest = None # best global position
        #
        # self.w, self.c1, self.c2 = 1, 2.05, 2.05
        #
        # self.r1 = random.random()
        # self.r2 = random.random()
        #
        # self.USE_PROGRAM_CONSTANTS = False
        # self.INDIVIDUAL_TYPE = Particle

    def initial_population(self):
        self.population = set()  # not a set in pso

        for i in range(self.POPULATION_SIZE):
            # current_v = list()
            # for t in self.program.get_input_types():
            #     current_v.append(random.randint(-self.vmax, self.vmax))

            harmony = Harmony(self.generate_random_data())    # a single random data based on types

            self.population.add(harmony)

        self.worst = self.get_worst_individual() #next(iter(self.pbest))
        self.after_initial_population()


    def iteration_operation_on_population(self):

        program_input_types = self.program.get_input_types()
        harmonies = list(self.population)
        for j in range(len(self.population)):

            if random.random() <= self.r_hmc:
                harmony = random.sample(self.population, 1)[0]

                inp = list(harmony.prog_input)
                for d in range(len(program_input_types)):
                    if random.random() <= self.r_pa:
                        if program_input_types[d] is int:
                            epsilon = random.uniform(-1, 1)
                            inp[d] = int(inp[d] + harmony.b_range * epsilon)
                        elif program_input_types[d] is bool:
                            epsilon = random.uniform(-1, 1)
                            if epsilon > 0:
                                inp[d] = not inp[d]
                harmony.prog_input = tuple(inp)

            else:
                harmony = Harmony(self.generate_random_data())

            harmonies[j] = harmony

        self.population.update(set(harmonies))
        self.keep_best_individuals()
        return self.population