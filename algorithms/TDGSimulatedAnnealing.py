import random
import time
from math import exp

from algorithms.TestDataGenerationAlgorithm import TestDataGenerationAlgorithm, Individual
from common.commons import Color


class TDGSimulatedAnnealing(TestDataGenerationAlgorithm):

    def __init__(self, program, goals):
        super().__init__(program, goals)

        #self.POPLUATION_SIZE = 1

    def initial_population(self):
        for i in range(self.POPULATION_SIZE):
            tup = self.generate_random_data()
            self.program.add_input(tup)

            self.population.add(Individual(tup))
        print("initial population: ", self.population)

    @staticmethod
    def get_temperature(iteration_count): # iteration_count ~ time
        k = 20
        lam = 0.045
        limit = 10000

        if iteration_count < limit:
            return k * exp(-lam * iteration_count)
        else:
            return 0

    def _probability_of_acceptance(self, temperature, delta_e):
        return exp(delta_e / temperature)

    # if /\E > 0 then current <- next
    # else current <- next only with probability e^(/\E/T)
    def _should_accept(self, temperature, delta_e):
        if delta_e > 0:
            return True
        else:
            return random.uniform(0, 1) <= self._probability_of_acceptance(temperature, delta_e)

    def neighbor(self, indv):

        maxdist = 1000#2**32 / 100

        newinp = []
        for d, type_ in enumerate(self.program.get_input_types()):
            if type_ == int:
                newinp.append(indv.prog_input[d] + random.randint(-maxdist // 2, maxdist // 2))
                # check for bounds:
                if newinp[d] > self.MAXINT:
                    newinp[d] = self.MAXINT
                if newinp[d] < self.MININT:
                    newinp[d] = self.MININT
            elif type_ == bool:
                newinp.append(random.randint(0, 1) == 1)

        return tuple(newinp)


    def generate_data(self):

        self.initial_population()

        while self.check_population_against_goals() <  \
                ((not self.TERMINATION_BY_TIME and self.iteration_count < self.MAX_ITERATION) \
                or (self.TERMINATION_BY_TIME and self.run_time <= self.MAX_TIME)):
            self.iteration_count += 1

            T = self.get_temperature(self.iteration_count)

            if T == 0:
                print('ERROR: T = 0')
                return  # if temperature = 0 then do nothing

            pop_list = list(self.population)
            for i, indv in enumerate(pop_list):
                self.neighbor(indv)

                # change if the new input is better:
                # delta_E: change in fitness:
                delta_E = self.fitness(indv) - self.fitness(pop_list[i])

                if self._should_accept(T, delta_E): # if delta_E > 0 (better fitness) or random (based on T)
                    pop_list[i] = indv

            self.population = set(pop_list)

            self.run_time = int(time.time()) - self.START_TIME

        # best found:
        last, fit = sorted([(i, self.fitness(i)) for i in self.population]
                           , key=lambda t: t[1], reverse=True)[0]

        print(Color.lightred, "last fitness: ", last, [fit], Color.reset)

        return self.iteration_count, [1 if fit == 1 else 0], [fit]