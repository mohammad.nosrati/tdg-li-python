from abc import ABC, abstractmethod

from common.SimilarityCalculator import SimilarityCalculator
from common.commons import *
from common.commons import Color
import time

from main.globals import get_global_seed


class Individual:
    def __init__(self, prog_input):
        self.prog_input = prog_input

    def __repr__(self):
        return "I:" + str(self.prog_input)

    def __hash__(self):
        return hash(self.prog_input)

    def __eq__(self, other):
        return self.prog_input == other.prog_input


class TestDataGenerationAlgorithm(ABC):
    def __init__(self, program, goals):
        self.verbose = False
        self.program = program

        if not isinstance(goals, list):
            goals = [goals]  # TODO make better
        self.goals = goals
        self.current_goal = goals[0]

        self.MULTIPLE_GOALS = len(self.goals) > 1

        self.population = set()           # set of individuals
        self.POPULATION_SIZE = 100

        self.TARGET_COUNT = 100
        self.MAX_ITERATION = 50

        self.MAX_TIME = 20
        self.START_TIME = 0
        self.run_time = 0
        self.TERMINATION_BY_TIME = True

        self.USE_PROGRAM_CONSTANTS = False
        self.USE_LI_FOR_INIT = False

        self.CONSTANT_FOR_INIT_PROB = 0.1

        # 32 bit ints
        self.BITS_COUNT = 32
        self.MININT = -2 ** self.BITS_COUNT + 1
        self.MAXINT = 2 ** self.BITS_COUNT - 1

        self.iteration_count = 0
        self.GOAL_PARTITION_SIZE = self.POPULATION_SIZE // len(self.goals)

        self.coverage_iteration_by_goal = dict()
        for g in self.goals:
            self.coverage_iteration_by_goal[g] = None


    @abstractmethod
    def generate_data(self):
        pass

    def covers_goal(self, p, g):
        return sim_jaccard(self.program.get_coverage(p.prog_input), g) > 0

    def check_population_against_goals(self):
        covered = 0

        for g in self.goals:
            any_covered = {p for p in self.population if sim_jaccard(self.program.get_coverage(p.prog_input), g) > 0} # can replace with ... if covers_goal(p, g)

            if len(any_covered) >= 1:
                covered += 1
                if self.coverage_iteration_by_goal[g] is None:
                    self.coverage_iteration_by_goal[g] = self.iteration_count

                print(Color.green, g, "COVERED :)", Color.lightblue)
                print("Real Coverage: ", self.program.get_coverage(next(iter(any_covered)).prog_input))
                print(any_covered)

            else:
                print(Color.red, g, "NOT COVERED :(", Color.lightblue)
        percent = covered / len(self.goals) * 100

        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
        print(f"{self.__class__.__name__}: {self.program.name}" )
        print(f"SEED: {get_global_seed()}")
        print(f"runtime: {self.run_time}")
        print("ITERATIONS: ", self.iteration_count)
        # print("COVERAGE BY ITERATION: ", ", ".join([str(int("".join([str(x) for x in k]), 2)) + ": " + str(v) \
        #     for k, v in self.coverage_iteration_by_goal.items()]))

        print("COVERAGE BY ITERATION: ", ", ".join([str(k) + ": " + str(v) \
            for k, v in self.coverage_iteration_by_goal.items()]))

        print("COVERAGE: ", percent, "%", "|" + '*' * int(percent / 2) +
              ' ' * (50 - int(percent / 2)) + "|", Color.reset)
        print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

        return percent

    def cover_goals(self):
        # for goal in self.goals:
        #     self.current_goal = goal
        #     self.population = set()
        #     print("goal: ", goal)
        self.population = set()
        self.iteration_count = 0

        self.START_TIME = int(time.time())
        self.run_time = 0

        counts, found_counts, best_fitnesses = self.generate_data()

        print(found_counts)
        print(best_fitnesses)
        print(Color.blue, "population: ", self.population, Color.reset)

        print("ITERATIONS: ", counts)
        print(f"TIME: {self.run_time}")

    def generate_random_data(self):
        vals = []

        for type_ in self.program.get_input_types():
            if type_ is int:
                if self.USE_PROGRAM_CONSTANTS:
                    cs = tuple(self.program.constants["numeric"])
                    if len(cs) > 0 and random.random() < self.CONSTANT_FOR_INIT_PROB:
                        val = int(random.choice(cs))
                    else:
                        val = random.randint(self.MININT, self.MAXINT)
                else:
                    val = random.randint(self.MININT, self.MAXINT)
                vals += [val]
            elif type_ is bool:
                vals += [random.randint(0,1) == 0]      # random True/False

        return tuple(vals)

    def _fitness(self, coverage, obj_vec):
        return SimilarityCalculator().sim_path(self.current_goal, coverage, obj_vec)

    def fitness(self, individual):    # fitness of individual according to self.current_goal
        return self._fitness(self.program.get_coverage(individual.prog_input), self.program.get_objectivefunc(individual.prog_input))

    def get_best_individual(self):
        # sorted_pop = list(self.population)
        # sorted_pop.sort(key=lambda p: self.fitness(self.program.get_coverage(p), self.program.get_objectivefunc(p)),
        #                 reverse=True)
        #
        # best = tuple(sorted_pop[0])
        #
        # return best
        return [p for p in sorted(self.population, key=lambda p: self.fitness(p), reverse=True)][0]

    def get_worst_individual(self):
        return [p for p in sorted(self.population, key=lambda p: self.fitness(p))][0]

    def clip(self, x):
        return max(self.MININT, min(self.MAXINT, x))