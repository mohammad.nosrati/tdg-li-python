import random

from algorithms.TDGGenetics import TDGGenetics
from common.commons import get_invariants_for_cov, Color


class TDGGeneticsWithLI(TDGGenetics):

    def __init__(self, program, goals):
        TDGGenetics.__init__(self, program, goals)

        self.USE_PROGRAM_CONSTANTS = True
        self.USE_LI_FOR_INIT = True

    def after_initial_population(self):
        if self.USE_LI_FOR_INIT:
            self.population = set(random.sample(self.population, self.POPULATION_SIZE * int(1 - self.INIT_WITH_LI_PROB)))

            self.new_population(self.generate_initial_populaton_with_li())

        print("INITIAL POP WITH LI: ", self.population)

    def after_iteration(self, best):

        coverage = self.program.get_coverage(best)  # TODO
        inv_best = get_invariants_for_cov(self.program.lig, coverage)[1]

        if self.verbose:
            print(Color.darkgrey, "invariants for best: ", inv_best, Color.reset)

        inputs_like_best = self.generate_values_for_inv(inv_best, coverage, self.NEW_INDIVIDUAL_COUNT)

        self.new_population(inputs_like_best) # TODO does not return anything anymore

    def after_iteration_multi(self, bests):
        for b in bests:
            print(Color.red, b, Color.reset)
            coverage = self.program.get_coverage(b.prog_input)
            inv_best = get_invariants_for_cov(self.program.lig, coverage)[1]

            if self.verbose:
                print(Color.darkgrey, "invariants for best: ", inv_best, Color.reset)

            inputs_like_best = self.generate_values_for_inv(inv_best, coverage, self.NEW_INDIVIDUAL_COUNT)
            self.new_population(inputs_like_best)

            inputs_like_mut_best = self.mutate_invs_and_generate_values(inv_best)
            self.new_population(inputs_like_mut_best)