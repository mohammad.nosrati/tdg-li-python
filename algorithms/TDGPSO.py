import random

from algorithms.TDGSearchBased import TDGSearchBased
from algorithms.TestDataGenerationAlgorithm import Individual


class Particle(Individual):
    vmax = 24

    def __init__(self, prog_input):
        self.prog_input = prog_input
        self.pbest = self.prog_input
        self.V = self.random_velocity()

    def zero_tuple(self):
        return tuple([0 for t in range(len(self.prog_input))])

    def random_velocity(self):
        return tuple([random.randint(-Particle.vmax, Particle.vmax) for t in range(len(self.prog_input))])

    # def __hash__(self):
    #     return hash((self.prog_input, self.V))
    #
    # def __eq__(self, other):
    #     return self.prog_input == other.prog_input and self.V == other.V

    def __repr__(self):
        return "P:" + str(self.prog_input)


class TDGPSO(TDGSearchBased):
    def __init__(self, program, goals):
        super().__init__(program, goals)

        self.gbest = None # best global position

        self.w, self.c1, self.c2 = 1, 2.05, 2.05

        self.r1 = random.random()
        self.r2 = random.random()

        self.USE_PROGRAM_CONSTANTS = False
        self.INDIVIDUAL_TYPE = Particle

    def initial_population(self):
        self.population = set()  # not a set in pso

        for i in range(self.POPULATION_SIZE):
            # current_v = list()
            # for t in self.program.get_input_types():
            #     current_v.append(random.randint(-self.vmax, self.vmax))

            particle = Particle(self.generate_random_data())    # a single random data based on types

            self.population.add(particle)

        self.gbest = self.get_best_individual().prog_input #next(iter(self.pbest))
        self.after_initial_population()


    def iteration_operation_on_population(self):
        for prtcl in self.population:
            if self.fitness(prtcl) == 1:
                continue
            newV = list(prtcl.V)
            newX = list(prtcl.prog_input) # tuple to list

            # loop through all the particles in population:
            program_input_types = self.program.get_input_types()
            for d in range(len(program_input_types)):

                # update velocity for dimension d:
                newV[d] = int(min(self.w * prtcl.V[d] +
                                    self.c1 * self.r1 * (prtcl.pbest[d] - prtcl.prog_input[d]) +
                                    self.c2 * self.r2 * (self.gbest[d] - prtcl.prog_input[d]),
                                    Particle.vmax))

                # update position for dimenstion d:
                if program_input_types[d] is int:
                    newX[d] += newV[d]
                    newX[d] = self.clip(newX[d])
                elif program_input_types[d] is bool:
                    if newV[d] > Particle.vmax / 2 or newV[d] < -Particle.vmax / 2:      # don't change for slow velocities, but change probabilistically for fast changes
                        newX[d] = random.choice([True, False])

            prtcl.prog_input = tuple(newX)
            prtcl.V = tuple(newV)

            self.program.add_input(prtcl.prog_input)

            f = self.fitness(prtcl)
            f_pbest = self.fitness(Particle(prtcl.pbest))
            f_gbest = self.fitness(Particle(self.gbest))

            if f > f_pbest:
                prtcl.pbest = prtcl.prog_input

            if f > f_gbest:
                self.gbest = prtcl.prog_input

        return self.population