import os
import re
import sqlite3
from pprint import pprint
from sqlite3 import Error

# from misc.archive.last_results import results

SQLITE_DB_PATH = "..\output\sqlite_results.db"
SQLITE_DB_PATH_NEW = "..\output\sqlite_results_new.db"

def create_connection(db_file):
    """ create a database connection to a SQLite database """
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)


def create_table(conn, create_table_sql):
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def insert(conn, sql):
    try:
        c = conn.cursor()
        c.execute(sql)
    except Error as e:
        print(e)


def query(conn, sql):
    try:
        cur = conn.cursor()
        cur.execute(sql)

        # rows = cur.fetchall()

        # return rows
        return cur
    except Error as e:
        print(e)

final_res = dict()

def parse_and_merge_thesis_output(s):
    rg = re.compile(r'(\d+)\s+\{([^{]+)\}')
    rg2 = re.compile(r"'([^']+)': (\d+)")

    ss = s.strip().split("\n")
    methodname = ss[0]
    # print(methodname)

    # get previous dicts for this method, or create new ones if previously they did not exist
    if final_res.get(methodname) is None:
        percents_by_seed = dict()
        itcounts_by_seed = dict()
    else:
        percents_by_seed = final_res.get(methodname)[0]
        itcounts_by_seed = final_res.get(methodname)[1]

    # parse one item:
    for i, t in enumerate(ss):
        t = t.strip()
        if i == 0:
            continue
        if t == 'iterations by run':
            cur_mode = 'it'
            continue
        if t == 'percents by run':
            cur_mode = 'pc'
            continue

        m = rg.search(t)
        itn = m.group(1)

        if percents_by_seed.get(itn) is None:
            percents_by_seed[itn] = dict()
            itcounts_by_seed[itn] = dict()

        rest = m.group(2)

        # parse rows:
        tt = rest.split(",")
        for z in tt:
            m2 = rg2.search(z.strip())
            mode = m2.group(1)
            n = float(m2.group(2))

            if cur_mode == 'pc':
                percents_by_seed[itn][mode] = n
            if cur_mode == 'it':
                # if n == 50:
                #     n = np.nan
                itcounts_by_seed[itn][mode] = n

    return methodname, percents_by_seed, itcounts_by_seed

def db_path(path=SQLITE_DB_PATH):
    db_path = os.path.join(os.path.dirname(__file__), path)
    return db_path

def insert_archived_results():

    conn = create_connection(db_path(path=SQLITE_DB_PATH_NEW))

    with conn:
        create_table(conn, "CREATE TABLE IF NOT EXISTS results("
                           "experiment varchar(20),"
                           "run int,"
                           "program varchar(50),"
                           "method varchar(20),"
                           "coverage float,"
                           "iterations int,"
                           "primary key(experiment, run, program, method))")

        for s in results:
            program, percents, itcounts = parse_and_merge_thesis_output(s)

            EXP_ID = "SAVED"

            for run, perc_by_alg in percents.items():
                for alg, perc in perc_by_alg.items():
                    q = f"INSERT OR REPLACE INTO results VALUES('{EXP_ID}', " \
                        f"{run}, '{program}', '{alg}', {perc}, {itcounts[run][alg]})"
                    # print(q)
                    insert(conn, q)

# insert_current_results()


def sqlite_insert_result(program, percents_by_seed, itcounts_by_seed, exp_id):
    conn = create_connection(db_path())

    with conn:
        create_table(conn, "CREATE TABLE IF NOT EXISTS results("
                           "experiment varchar(20),"
                           "run int,"
                           "program varchar(50),"
                           "method varchar(20),"
                           "coverage float,"
                           "iterations int,"
                           "primary key(experiment, run, program, method))")


        for run, perc_by_alg in percents_by_seed.items():
            for alg, perc in perc_by_alg.items():
                q = f"INSERT OR REPLACE INTO results VALUES('{exp_id}', " \
                    f"{run}, '{program}', '{alg}', {perc}, {itcounts_by_seed[run][alg]})"
                print(q)
                insert(conn, q)

def sqlite_insert_result_new(exp_id, run, program, alg, cov_perc, iterations, run_time):#program, percents_by_seed, itcounts_by_seed, exp_id):
    conn = create_connection(db_path(SQLITE_DB_PATH_NEW))

    with conn:
        create_table(conn, "CREATE TABLE IF NOT EXISTS results("
                           "experiment varchar(20),"
                           "run int,"
                           "program varchar(50),"
                           "method varchar(20),"
                           "coverage float,"
                           "iterations int,"
                           "runtime int, "
                           "primary key(experiment, run, program, method))")

        q = f"INSERT OR REPLACE INTO results VALUES('{exp_id}', " \
            f"{run}, '{program}', '{alg}', {cov_perc}, {iterations}, {run_time})"
        print(q)
        insert(conn, q)


def sqlite_table_to_dict(new_db=False):

    conn = create_connection(db_path(path=SQLITE_DB_PATH_NEW if new_db else SQLITE_DB_PATH))

    final_res = dict()

    with conn:
        curs = query(conn, "SELECT * FROM results")

        while True:
            row = curs.fetchone()
            if not row:
                break
            program = row[2]
            alg = row[3]
            run = row[1]


            if final_res.get(program) is None:
                percents_by_seed = dict()
                itcounts_by_seed = dict()
            else:
                percents_by_seed = final_res.get(program)[0]
                itcounts_by_seed = final_res.get(program)[1]

            if percents_by_seed.get(run) is None:
                percents_by_seed[run] = dict()
                itcounts_by_seed[run] = dict()

            percents_by_seed[run][alg] = row[4]
            itcounts_by_seed[run][alg] = row[5]

            final_res[program] = percents_by_seed, itcounts_by_seed

        # pprint(final_res)
        return final_res

# sqlite_table_to_dict()