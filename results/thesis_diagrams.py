from scipy.stats import stats

from common.diagrams import plot_graphs, generate_COV_IT_vectors
import re
import numpy as np
from pandas import DataFrame

from common.stats import anovatest


# def input_lines():
#     lines = []
#     while True:
#         try:
#             lines.append(input())
#         except EOFError:
#             break
from misc.archive.last_results import results

PLOT = True
SUMMARY = True
STAT = True


final_res = dict()
all_algs = ["ga:single", "ga:multi", "ga-li", "pso", "pso-li", "random", "sa", "aco", "aco-es", "harmony"]


def parse_and_merge_thesis_output(s):
    rg = re.compile(r'(\d+)\s+\{([^{]+)\}')
    rg2 = re.compile(r"'([^']+)': (\d+)")

    ss = s.strip().split("\n")
    methodname = ss[0]
    print(methodname)

    # get previous dicts for this method, or create new ones if previously they did not exist
    if final_res.get(methodname) is None:
        percents_by_seed = dict()
        itcounts_by_seed = dict()
    else:
        percents_by_seed = final_res.get(methodname)[0]
        itcounts_by_seed = final_res.get(methodname)[1]

    # parse one item:
    for i, t in enumerate(ss):
        t = t.strip()
        if i == 0:
            continue
        if t == 'iterations by run':
            cur_mode = 'it'
            continue
        if t == 'percents by run':
            cur_mode = 'pc'
            continue

        m = rg.search(t)
        itn = m.group(1)

        if percents_by_seed.get(itn) is None:
            percents_by_seed[itn] = dict()
            itcounts_by_seed[itn] = dict()

        rest = m.group(2)

        # parse rows:
        tt = rest.split(",")
        for z in tt:
            m2 = rg2.search(z.strip())
            mode = m2.group(1)
            n = float(m2.group(2))

            if cur_mode == 'pc':
                percents_by_seed[itn][mode] = n
            if cur_mode == 'it':
                # if n == 50:
                #     n = np.nan
                itcounts_by_seed[itn][mode] = n

    print(percents_by_seed)
    print(itcounts_by_seed)

    final_res[methodname] = percents_by_seed, itcounts_by_seed

def dict_to_table(percents_by_run, all_algs):
    B = []
    for seed in sorted(percents_by_run.keys()):
        row = []
        for col in all_algs:
            if col in percents_by_run[seed]:
                row.append(percents_by_run[seed][col])
            else:
                row.append(np.NaN)
        B.append(row)
    return np.array(B)

cov_latex = []
it_latex = []




def interweave_arrays(a, b, c):
    d = np.empty((a.size + b.size + c.size,), dtype=a.dtype)
    d[0::3] = a
    d[1::3] = b
    d[2::3] = c
    return d


def tables(methodname, percents_by_run, itcount_by_run, result_table, all_algs, median=False):
    # algs = {0: "ga:single", 1: "ga:multi", 2: "ga-li", 3: "pso", 4: "pso-li", 5: "random", 6: "sa", 7: "aco", 8: "aco-es", 9: "harmony"}
    algs = {k:v for k, v in enumerate(all_algs)}
    fmt = "|".join(["{:20}"] * len(algs)) # something like: {:20}|{:20}|{:20}
    print(fmt.format(*list(algs.values())))

    C = dict_to_table(percents_by_run, all_algs)

    # for coverages:
    print("----------- coverages ---------" + methodname)

    if median:
        COVMEAN = np.nanmedian(C, axis=0)# C.mean(axis=0)
    else:
        COVMEAN = np.nanmean(C, axis=0)
    print("median" if median else "mean", COVMEAN)
    FULL_COV = np.count_nonzero(C == 100, axis=0) / C.shape[0]
    print("100% percent", FULL_COV)

    with np.warnings.catch_warnings():
        pass

    FULL_COV = FULL_COV * 100

    D = np.vstack([FULL_COV, COVMEAN])
    print("D: ")
    print(D)

    print()
    E = D.flatten('F')

    # lrow = " & ".join(["{:.3g}".format(n) for n in E])
    # cov_latex.append("\\textbf{" + methodname + "} & " + lrow + "\\\\")


    # for iterations:
    print("----------- iterations ---------" + methodname)
    C = dict_to_table(itcount_by_run, all_algs)
    #C[np.logical_and(C > 0, C == C.max())] = np.nan


    # print("E", E)
    #E = np.nanmin(C, axis=0)
    # E = np.vstack([E, np.nanmax(C, axis=0)])
    #E = np.vstack([E, np.nanmean(C, axis=0)])

    if median:
        ITMEAN = np.nanmedian(C, axis=0)#np.nanmean(C, axis=0)
    else:
        ITMEAN = np.nanmean(C, axis=0)

    print(ITMEAN)
    ITMEAN = ITMEAN.flatten('F')

    # lrow = " & ".join(["-" if np.isnan(n) else "{:.3g}".format(n)
    #                   for n in ITMEAN])
    # lrow = "\\textbf{" + methodname + "} & " + lrow + "\\\\"
    # it_latex.append(lrow)


    print("last result: ")
    table = interweave_arrays(COVMEAN, FULL_COV, ITMEAN)

    lrow = " & ".join(["-" if np.isnan(n) else "{:.4g}".format(n)
                      for n in table])
    lrow = "\\textbf{" + methodname.replace("_", "\\_") + "} & " + lrow + "\\\\"

    print(fmt.format(*list(algs.values())))

    result_table.append(lrow)


def generate_stats(final_res, cov, all_algs, target_alg, excludes=[]):

    if cov:
        print("COVERAGES: ")
    else:
        print("ITERATIONS: ")

    stat_tbl = []

    aliases = 'bcdefghijklmnopqrstuvwxyz'
    for method in final_res.keys():
        if method in excludes:
            continue
        print('--------------')
        RUN, COV, IT = generate_COV_IT_vectors(final_res[method][1], all_algs, final_res[method][0])

        col_names = ""
        cols = ""
        for i, alg in enumerate(all_algs):
            if alg != target_alg:
                print(method, alg)
                if cov:
                    h0_rejected, m_d, p = anovatest(COV[alg], COV[target_alg], alg, target_alg)
                else:
                    h0_rejected, m_d, p = anovatest(IT[target_alg], IT[alg], target_alg, alg)

                cols += f"& {m_d:.3g} & {p:.3g} "
                alias = aliases[i]
                col_names += f" & \\multicolumn{2}{{|c|}}{{\\textbf{{{alg}(${alias}$)}}}}\n"

        mth = method.replace("_", "\\_")

        stat_tbl.append(f"\\textbf{{{mth}}} {cols} \\\\\n\\hline")
    print(f"\nStatistic Analysis of {target_alg} against others")
    print(col_names)
    for i in range(len(all_algs) - 1):
        print(f" & $m_a - m_{aliases[i]}$ & \\textit{{p-value}}")
    for r in stat_tbl:
        print(r)


if __name__ == "__main__":
    excludes = {'leapyear'}
    for s in results:
        parse_and_merge_thesis_output(s)
        print("---------------------------")

    if PLOT:
        for method in final_res.keys():
            if method not in excludes and method != 'mid':
                plot_graphs(method, final_res[method][0], all_algs, final_res[method][1],
                            False)  # final_res: dict of tuples (key: methodname, tuple: pc, and it dicts)

    result_table = []

    if SUMMARY:
        for method in final_res.keys():
            if method not in excludes:
                tables(method, final_res[method][0], final_res[method][1], result_table, all_algs)
        # print("coverages:")
        # for c in cov_latex:
        #     print(c)
        #     print("\\hline")
        #
        # print("\niterations: ")
        # for i in it_latex:
        #     print(i)
        #     print("\\hline")

        print("\neverything: ")
        for i in result_table:
            print(i)
            print("\\hline")

    stat_tbl = []
    if STAT:
        generate_stats(final_res, True, all_algs, "ga-li", excludes=excludes)
        print("--------------------")
        generate_stats(final_res, False, all_algs, "ga-li", excludes=excludes)


