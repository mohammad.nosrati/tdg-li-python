from common.diagrams import plot_graphs
from results.sqlite_results import sqlite_table_to_dict
from results.thesis_diagrams import generate_stats, tables

SUMMARY = True # False
STAT = True    # False
DIAGRAM = False # False

# SAVE_PLOT = False    # False


# STAT_tbl = "gp"
STAT_tbl = "ga-li"

# SUBFOLDER = "gp"
SUBFOLDER = "li"

NEWDB = True

MEDIAN = True

def check_data_integrity():
    global run, alg
    for k, v in final_res.items():
        if k not in excludes:
            for run in range(30):
                for alg in all_algs:
                    if run not in final_res[k][0]:
                        print(f"ERROR - percents (run): method {k}, algorithm {alg}, run {run}")
                        continue
                    elif alg not in final_res[k][0][run]:
                        print(f"ERROR - percents (alg): method {k}, algorithm {alg}, run {run}")
                        continue

                    if final_res[k][0][run][alg] is None:
                        print(f"ERROR - percents (None): method {k}, algorithm {alg}, run {run}")
                    if run not in final_res[k][1]:
                        print(f"ERROR - iterations (run): method {k}, algorithm {alg}, run {run}")
                        continue
                    elif alg not in final_res[k][1][run]:
                        print(f"ERROR - iterations (alg): method {k}, algorithm {alg}, run {run}")
                        continue

                    if final_res[k][1][run][alg] is None:
                        print(f"ERROR - percents (None): method {k}, algorithm {alg}, run {run}")


all_algs = [
    "ga:single",
    "ga:multi",
    "ga-li",
    "pso",
    "pso-li",
    "random",
    "sa",
    "aco",
    # "aco-es",
    "harmony",

    # "gp"
]

if DIAGRAM or SUMMARY or STAT:
    ## final_res[method][0] : percents
    ## final_res[method][1] : counts

    ## final_resul[method][0][run][alg]  # percent for method 'method', run 'run' and algorithm 'alg'

    final_res = sqlite_table_to_dict(new_db=NEWDB)
    excludes = ["bessj"]

    check_data_integrity()

    if DIAGRAM:

        for method in final_res.keys():
            if method not in excludes:
                print(f"GRAPH: {method}")
                plot_graphs(method, final_res[method][0], all_algs, final_res[method][1],
                            False,
                            subfolder=SUBFOLDER)  # final_res: dict of tuples (key: methodname, tuple: pc, and it dicts)

    result_table = []
    if SUMMARY:
        for method in final_res.keys():
            if method not in excludes:
                tables(method, final_res[method][0], final_res[method][1], result_table, all_algs, median=MEDIAN)

        print("\neverything: ")
        for i in result_table:
            print(i)
            print("\\hline")

    stat_tbl = []
    if STAT:
        generate_stats(final_res, True, all_algs, STAT_tbl, excludes=excludes)
        print("--------------------")
        generate_stats(final_res, False, all_algs, STAT_tbl, excludes=excludes)