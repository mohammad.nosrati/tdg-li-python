import datetime
import math
import random

from algorithms.TDGACO_ES import TDGACO_ES
from algorithms.TDGACO import TDGACO
from algorithms.TDGGenetics import TDGGenetics
from algorithms.TDGGeneticsWithLI import TDGGeneticsWithLI
from algorithms.TDGHarmony import TDGHarmony
from algorithms.TDGPSO import TDGPSO
from algorithms.TDGPSOWithLI import TDGPSOWithLI
from algorithms.TDGRandom import TDGRandom
from algorithms.TDGSimulatedAnnealing import TDGSimulatedAnnealing
from common.Instrumenter import Instrumenter
from common.diagrams import plot_graphs
from common.sample_programs import gcd, numOfDays, mid, triangle, dayOfWeek, tcas, leapyear, quadratic_equation, \
    point_circle, bessj, isValidDate, compute_tax, print_calendar, line_rectangle, fisher
from main.globals import set_global_seed
from results.sqlite_results import sqlite_insert_result, sqlite_insert_result_new

SAVE_RESULTS = False
MAX_ITERATIONS = 50
MAX_TIME = 20
# MAX_ITERATIONS = 1
NUM_OF_RUNS = 10
# NUM_OF_RUNS = 1
DIAGRAM = False
TERMINATION_BY_TIME = True

algs = {
            "ga:single": False,
            "ga:multi": False,
            "ga-li": False,
            "pso": False,
            "pso-li": False,
            "random": False,
            "sa": False,
            "aco": False,
            "aco-es": False,
            "bee": False,
            "harmony": False,
            "cuckoo": False
}

meths = {(mid, (int, int, int), MAX_ITERATIONS): False,
             (gcd, (int, int), MAX_ITERATIONS): False,
             (triangle, (int, int, int), MAX_ITERATIONS): False,
             (dayOfWeek, (int, int, int), MAX_ITERATIONS): False,
             (numOfDays, (int, int, int, int, int), MAX_ITERATIONS): False,
             (tcas, (int, bool, bool, int, int, int, int, int, int, int, int, bool), MAX_ITERATIONS): False,
             (leapyear, (int,), MAX_ITERATIONS): False,
             (quadratic_equation, (int, int, int), MAX_ITERATIONS): False,
             (point_circle, (int, int, int), MAX_ITERATIONS): False,
             (isValidDate, (int, int, int), MAX_ITERATIONS): False,
             (bessj, (int, int), MAX_ITERATIONS): False,
             (compute_tax, (int, int), MAX_ITERATIONS): False,
             (print_calendar, (int,), MAX_ITERATIONS): False,
             (line_rectangle, (int,int, int, int, int, int, int ,int), MAX_ITERATIONS): False,
             (fisher, (int,int, int), MAX_ITERATIONS): False,
         }

def set_algs_meths(algs, alg_sub, meths, meths_sub):
    for aa in alg_sub:
        algs[aa] = True
    for mm in meths_sub:
        for meth in meths.keys():
            if meth[0].__name__ == mm:
                meths[meth] = True

def setup_experiment(algs, meths):
    # set_algs_meths(algs, {"ga-li"}, meths, {"triangle"})
    # set_algs_meths(algs, {"random"}, meths, {"tcas"})
    # set_algs_meths(algs, {"ga-li", "pso-li", "aco-es"}, meths, {"leapyear"})
    # set_algs_meths(algs, {"harmony"}, meths, {"leapyear"})
    # set_algs_meths(algs, {"random"}, meths, {"triangle", "gcd", "mid", "dayOfWeek", "numOfDays", "tcas", "quadratic_equation", "point_circle", "isValidDate"})

    # set_algs_meths(algs, {"ga:multi"}, meths, {"print_calendar"})
    set_algs_meths(algs, {
        # "ga:single",
        # "ga:multi",
        # "pso",
        # "random",
        # "sa",
        # "aco",
        # "harmony",
        "ga-li",
        # 'pso-li'
    },
                   meths, {#"compute_tax",

                            # "line_rectangle",
                            #"fisher",
                        # "gcd",
                        "quadratic_equation",
                        "triangle",
                        # "numOfDays",
                        # "dayOfWeek",
                        # "isValidDate",
                        # "point_circle",
                        # "bessj",
                        # "tcas",
                       # "print_calendar",
                       # "compute_tax",
                       # "fisher",
                        })
    # set_algs_meths(algs, {"ga:multi"}, meths, {"compute_tax"})


def cover_all(seed, name, method, args, its, percents_by_seed, itcounts_by_seed, runtimes_by_seed):

    met_inst = Instrumenter().instrument(method, args)
    b = met_inst.branch_count
    goals = list()
    for i in range(b):
        br = [0] * b
        br[i] = 1
        goals.append(tuple(br))

    if name == "ga-li":
        tdg = TDGGeneticsWithLI(met_inst, goals)

        # tdg.verbose = True
        tdg.MAX_ITERATION = its
        tdg.MAX_TIME = MAX_TIME
        tdg.TERMINATION_BY_TIME = TERMINATION_BY_TIME
        tdg.cover_goals()

        percents_by_seed[seed][name] = tdg.check_population_against_goals()
        itcounts_by_seed[seed][name] = tdg.iteration_count
        runtimes_by_seed[seed][name] = tdg.run_time

    elif name == "pso-li":
        tdg = TDGPSOWithLI(met_inst, goals)

        # tdg.verbose = True
        tdg.MAX_ITERATION = its
        tdg.MAX_TIME = MAX_TIME
        tdg.TERMINATION_BY_TIME = TERMINATION_BY_TIME
        tdg.cover_goals()

        percents_by_seed[seed][name] = tdg.check_population_against_goals()
        itcounts_by_seed[seed][name] = tdg.iteration_count
        runtimes_by_seed[seed][name] = tdg.run_time

    elif name == "ga:multi":
        tdg = TDGGenetics(met_inst, goals)

        # tdg.verbose = True
        tdg.MAX_ITERATION = its
        tdg.MAX_TIME = MAX_TIME
        tdg.TERMINATION_BY_TIME = TERMINATION_BY_TIME

        tdg.USE_PROGRAM_CONSTANTS = False
        tdg.cover_goals()

        percents_by_seed[seed][name] = tdg.check_population_against_goals()
        itcounts_by_seed[seed][name] = tdg.iteration_count
        runtimes_by_seed[seed][name] = tdg.run_time

    # single goals:
    elif name == "ga:single" or name == "pso" or name == "random" or name == "sa" or name == "aco" or name == "aco-es" \
        or name == "bee" or name == "harmony" or name == "cuckoo":
        tdg = single_goal(goals, itcounts_by_seed, its, met_inst, name, percents_by_seed, runtimes_by_seed, seed)

    tdg.check_population_against_goals()


def single_goal(goals, itcounts_by_seed, its, met_inst, name, percents_by_seed, runtimes_by_seed, seed):
    how_many = 0
    total_time = 0
    total_iterations = 0

    max_it = -1
    for subgoal in goals:
        if name == "ga:single":
            tdg = TDGGenetics(met_inst, subgoal)
        elif name == "pso":
            tdg = TDGPSO(met_inst, subgoal)
        elif name == "random":
            tdg = TDGRandom(met_inst, subgoal)
        elif name == "sa":
            tdg = TDGSimulatedAnnealing(met_inst, subgoal)
        elif name == "aco":
            tdg = TDGACO(met_inst, subgoal)
        elif name == "aco-es":
            tdg = TDGACO_ES(met_inst, subgoal)
        elif name == "harmony":
            tdg = TDGHarmony(met_inst, subgoal)
        elif name == "bee":
            pass
        elif name == "cuckoo":
            pass

        tdg.MAX_ITERATION = its
        tdg.MAX_TIME = math.ceil(MAX_TIME / len(goals))
        tdg.TERMINATION_BY_TIME = TERMINATION_BY_TIME

        # tdg.verbose = True
        tdg.USE_PROGRAM_CONSTANTS = False
        tdg.cover_goals()

        if tdg.check_population_against_goals() == 100:
            how_many += 1

        max_it = tdg.iteration_count if tdg.iteration_count > max_it else max_it
        percents_by_seed[seed][name] = how_many / len(goals) * 100

        total_time += tdg.run_time
        total_iterations += tdg.iteration_count

    runtimes_by_seed[seed][name] = total_time
    itcounts_by_seed[seed][name] = total_iterations if tdg.TERMINATION_BY_TIME else max_it # TODO?
    return tdg


def output():
    global SAVE_RESULTS

    print(method.__name__, percents_by_seed)
    print(method.__name__, itcounts_by_seed)

    if SAVE_RESULTS:
        # dt = str(datetime.datetime.now().date()) + " " + str(datetime.datetime.now().time())

        # with open("output/li-results.txt", "a") as results_file:
        #     results_file.write(method.__name__ + "\n")
        #     results_file.write("percents by run\n")
        #     for k, v in percents_by_seed.items():
        #         results_file.write("{} {}\n".format(k, v))
        #
        #     # results_file.write(method.__name__ + " " + str(itcounts_by_seed) + "\n")
        #     results_file.write("iterations by run\n")
        #     for k, v in itcounts_by_seed.items():
        #         results_file.write("{} {}\n".format(k, v))

            ## save everything together:
            # sqlite_insert_result(method.__name__, percents_by_seed, itcounts_by_seed, dt)

        if DIAGRAM:
            plot_graphs(method.__name__, percents_by_seed, names, itcounts_by_seed, False)


if __name__ == "__main__":
    # global algs, meths, SAVE_RESULTS

    setup_experiment(algs, meths)
    
    names = [k for (k, v) in algs.items() if v]
    methods = [k for (k, v) in meths.items() if v]

    print(names)
    print(methods)

    if SAVE_RESULTS:
        dt = dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        # with open("output/li-results.txt", "a") as results_file:
        #     results_file.write("\n\n" + dt + "\n")

    for m in methods:
        method, args, its = m[0], m[1], m[2]

        percents_by_seed = dict()
        itcounts_by_seed = dict()
        runtimes_by_seed = dict()

        for seed in range(0, NUM_OF_RUNS):
            print("~~~~~ SEED: ", seed, "~~~~~~")
            set_global_seed(seed)
            random.seed(seed)

            percents_by_seed[seed] = dict()
            itcounts_by_seed[seed] = dict()
            runtimes_by_seed[seed] = dict()
            for n in names:
                percents_by_seed[seed][n] = -1
                itcounts_by_seed[seed][n] = -1
                runtimes_by_seed[seed][n] = 0

            for n in names:
                print('****************', n, '*********************')
                cover_all(seed, n, method, args, its, percents_by_seed, itcounts_by_seed, runtimes_by_seed)
                print("--------------------------------------------")

                if SAVE_RESULTS:
                    dt = dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

                    filtered_percents_by_seed, filtered_itcounts_by_seed = dict(), dict()
                    filtered_percents_by_seed[seed] = dict()
                    filtered_itcounts_by_seed[seed] = dict()

                    filtered_percents_by_seed[seed][n] = percents_by_seed[seed][n]
                    filtered_itcounts_by_seed[seed][n] = itcounts_by_seed[seed][n]

                    #sqlite_insert_result(method.__name__, filtered_percents_by_seed, filtered_itcounts_by_seed, dt)
                    sqlite_insert_result_new(dt, seed, method.__name__, n, percents_by_seed[seed][n],
                                             itcounts_by_seed[seed][n], runtimes_by_seed[seed][n])

        output()
