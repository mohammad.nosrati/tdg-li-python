
GLOBAL_SEED = 0

def set_global_seed(s):
    global GLOBAL_SEED
    GLOBAL_SEED = s

def get_global_seed():
    global GLOBAL_SEED
    return GLOBAL_SEED